﻿<%@ Page Title="CMS :: Change Password" Language="C#" MasterPageFile="~/SiteLogin.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Fleet_Card.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <div id="loginbox" class="login-box">
        <div class="login-box-body">
            <div class="login-logo">
                <h3 style="color: white;"><b>Car &nbsp Management &nbsp System</b></h3>
            </div>
            <p class="login-box-msg">Change your password</p>
            <div class="form-group has-feedback">
                <input runat="server" id="username" clientidmode="static" class="form-control" onkeypress="return allowOnlyNumber(event);" placeholder="Employee ID" maxlength="8" tabindex="1" autocomplete="off" data-toggle="tooltip" data-placement="left" title="กรอกหมายเลขพนักงานของท่าน">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input runat="server" id="old_password" clientidmode="static" type="password" class="form-control" placeholder="Old Password" autocomplete="new-password" data-toggle="tooltip" data-placement="left" title="กรอกรหัสผ่านเก่าของท่าน" maxlength="16" tabindex="2">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input runat="server" id="new_password" clientidmode="static" type="password" class="form-control" placeholder="New Password" autocomplete="new-password" data-toggle="tooltip" data-placement="left" title="กรอกรหัสผ่านใหม่ของท่าน" maxlength="16" tabindex="3">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input runat="server" id="re_new_password" clientidmode="static" type="password" class="form-control" placeholder="Retype New Password" autocomplete="new-password" data-toggle="tooltip" data-placement="left" title="กรอกรหัสผ่านใหม่ของท่านอีกครั้ง" maxlength="16" tabindex="4">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input runat="server" id="Email" type="email" clientidmode="static" class="form-control" placeholder="E-Mail" data-toggle="tooltip" data-placement="left" title="กรอกที่อยู่อีเมล์ของท่าน" tabindex="5">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-5 col-xs-offset-1">
                    <asp:Button ID="Confrim" runat="server" ClientIDMode="Static" Text="Confrim" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="Confrim_Click" TabIndex="6" />
                </div>


                <div class="col-xs-5">
                    <a href="Login.aspx">
                        <input type="button" class="btn btn-danger btn-block btn-flat" value="Back" tabindex="7" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <script>

        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'รหัสพนักงานผิด\nกรุณาตรวจสอบรหัสพนักงานให้ถูกต้อง';
                    break;
                case 1:
                    MessageAlert = 'Fatal Error : cant changing your password (get form sql)\nplease contact system administrator.';
                    break;
                case 2:
                    MessageAlert = 'แก้ไขรหัสผ่านของท่านเสร็จเรียบร้อยแล้ว';
                    break;
                case 3:
                    MessageAlert = 'Fatal Error : cant changing your password (update to sql)\nplease contact system administrator.';
                    break;
                case 4:
                    MessageAlert = 'รหัสผ่านเก่าของท่านผิด\nกรุณาตรวจสอบรหัสผ่านเก่าของท่านอีกครั้งหนึ่ง';
                    break;
            }
            alert(MessageAlert);
            if (typeAlert == 2){
                window.location.replace("Login.aspx");
            }
        }

        function checknotchoose() {
            var empid = $('#username').val();
            var oldpass = $('#old_password').val();
            var NewPass = $('#new_password').val();
            var ReNewPass = $('#re_new_password').val();
            var mail = $('#Email').val();

            if ($.trim(empid) == "") {
                alert('กรุณากรอกรหัสพนักงาน');
                $('#username').focus();
                $('#username').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#username').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (oldpass == "") {
                alert('กรุณากรอกรหัสผ่านเก่า');
                $('#old_password').focus();
                $('#old_password').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#old_password').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (NewPass == "") {
                alert('กรุณากรอกรหัสผ่านใหม่');
                $('#new_password').focus();
                $('#new_password').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#new_password').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (NewPass.length < 8 || NewPass.length > 16) {
                alert('กรุณากรอกรหัสผ่านใหม่\nให้มีความยาวมากกว่า 8 ตัวอักษร\nและน้อยกว่า 16 ตัวอักษร');
                $('#new_password').focus();
                $('#new_password').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#new_password').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }

            else if (ReNewPass == "") {
                alert('กรุณากรอกรหัสผ่านใหม่ครั้งที่สอง');
                $('#re_new_password').focus();
                $('#re_new_password').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#re_new_password').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (mail == "") {
                alert('กรุณากรอกที่อยู่อีเมล์ของท่าน');
                $('#Email').focus();
                $('#Email').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#Email').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (NewPass != ReNewPass) {
                alert('กรุณากรอกรหัสผ่านใหม่ทั้งสองครั้งให้ตรงกัน');
                $('#new_password').focus();
                $('#new_password').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#new_password').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }

            else {
                return true;
            }

        }

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57) {
                return false;
            }

        }

    </script>
</asp:Content>
