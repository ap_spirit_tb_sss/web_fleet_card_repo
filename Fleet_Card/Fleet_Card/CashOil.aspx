﻿<%@ Page Title="CMS :: Cash for Oil" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CashOil.aspx.cs" Inherits="Fleet_Card.CashOil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainForm")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("CashOil")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link4")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .borderset {
            border-width: 1px;
            border-radius: 7px;
            border-color: grey;
            border-style: solid;
        }

        .select {
            color: #9e9e9e !important;
        }

        .option:not(:first-of-type) {
            color: black !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <link href="dist/calendarextenderdisable/CSS.css" rel="stylesheet" type="text/css" />
    <script src="dist/calendarextenderdisable/Extension.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <%--//Titleหัวข้อ--%>
    <section class="content-header">
        <h1>แบบฟอร์มขอเติมน้ำมันเป็นเงินสด
            <small>(beta)</small>
        </h1>
        <div style="margin-top: 10px;">
            <div style="margin-bottom: 10px;">
                <asp:Label ID="LabelDescript" runat="server" Text="กรุณากรอกข้อมูล ในช่องต่างๆให้ครบถ้วนและถูกต้อง"></asp:Label>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="form-group has-feedback col-sm-12" style="text-align: center;">
                        <h3>หนังสือขออนุมัติเติมน้ำมันเป็นเงินสด </h3>
                    </div>
                    <div class="form-group has-feedback col-sm-12" style="text-align: center;">
                        <h4>สาขา <%=Session["Workplace"].ToString() %> &nbsp&nbsp ประจำวันที่ <%= DateTime.Now.Date.ToString("dd/MM/yyyy") %> </h4>
                    </div>
                    <div class="form-group has-feedback col-sm-1" style="text-align: center;">
                    </div>
                    <div class="form-group has-feedback col-sm-11" style="padding-bottom: 30px;">
                        <label class="form-group has-feedback col-sm-12" style="font-weight: normal;">
                            เนื่องด้วยข้าพเจ้า &nbsp&nbsp <b><u><%=Session["Name"].ToString() %>&nbsp&nbsp<%=Session["LastName"].ToString() %></u></b>&nbsp&nbsp&nbsp
                            ตำแหน่ง &nbsp&nbsp&nbsp<b><u><%=Session["EmpPosition"] %></u></b>&nbsp&nbsp&nbsp

                       <%-- </label>
                        <label class="form-group has-feedback col-sm-12" style="font-weight: normal;">--%>
                            หน่วยงาน &nbsp&nbsp&nbsp <b><u><%=Session["Org"] %></u></b>&nbsp&nbsp&nbsp
                            พื้นที่/หน่วยขาย &nbsp&nbsp&nbsp <b><u><%=Session["Workplace"] %></u></b>&nbsp&nbsp&nbsp 
                        </label>
                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label1" runat="server" Text="ขับรถยนต์หมายเลขทะเบียน<font color='red'>*</font>"></asp:Label>
                                <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="borderset" runat="server" AppendDataBoundItems="True" TabIndex="1">
                                    <%--OnSelectedIndexChanged="CarPlate_SelectedIndexChanged"--%>
                                    <Items>
                                        <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                    </Items>
                                </asp:DropDownList>
                            </div>
                            <div style="float: left; padding-left: 10px;padding-top:6px;">
                                <asp:Label ID="Label7" runat="server" Text="ยี่ห้อ"></asp:Label>
                                <asp:TextBox ID="CarBrand" ClientIDMode="Static" CssClass="borderset" runat="server" Style="text-align: center;" />
                            </div>
                            <div style="float: left; padding-left: 10px;">
                                <asp:Label ID="Label8" runat="server" Text="โดยได้รับวงเงินตามการ์ด จำนวน<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="CardLimit" ClientIDMode="Static" CssClass="borderset allownumericwithdecimal" runat="server" Style="text-align: center; width: 80px;" />
                                <asp:Label ID="Label9" runat="server" Text="ลิตร"></asp:Label>
                            </div>

                        </div>

                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label10" runat="server" Text="โดยจะขออนุมัติเติมน้ำมันเป็นเงินสดจำนวน<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="OilMoney" ClientIDMode="Static" CssClass="borderset allownumericwithdecimal" runat="server" Style="text-align: center; width: 80px;" />
                                <asp:Label ID="Label11" runat="server" Text="บาท (ระบุจำนวน<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="OilLitre" ClientIDMode="Static" CssClass="borderset allownumericwithdecimal" runat="server" Style="text-align: center; width: 80px;" />
                                <asp:Label ID="Label12" runat="server" Text="ลิตร)"></asp:Label>
                            </div>
                            <div style="float: left; padding-left: 10px;">
                                <asp:Label ID="Label3" runat="server" Text="ภายในวันที่<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="DateOil" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" ClientIDMode="Static" />
                                <%--onchange="buildEndDate();"--%>
                            </div>
                        </div>
                        <div class="form-group has-feedback col-sm-3" style="padding-top: 7px;">
                            <asp:Label ID="Label4" runat="server" Text="กรุณาเลือกภาพใบเสร็จค่าน้ำมัน<font color='red'>*</font>"></asp:Label>
                        </div>
                        <div class="form-group has-feedback col-sm-5">
                            <asp:FileUpload ID="BillUpload" ClientIDMode="Static" runat="server" CssClass="form-control" ViewStateMode="Enabled" accept="image/jpg,image/jpeg,image/png" OnChange="return checkFileExtension(this,'BillUpload');" multiple="false" />
                        </div>
                        <div class="form-group has-feedback col-sm-12" style="float: left;">
                            <asp:Label ID="Label2" runat="server" Text="โดยมีรายละเอียดสาเหตุในการขออนุมัติดังนี้<font color='red'>*</font>"></asp:Label>
                        </div>
                        <div class="form-group has-feedback col-sm-12">
                            <asp:TextBox ID="ReasonText" runat="server" CssClass="borderset" ClientIDMode="Static" TextMode="MultiLine" Style="resize: none; width: 80%; height: 300px;" TabIndex="6" />
                        </div>

                    </div>

                </div>
            </div>

            <div class="form-group has-feedback col-sm-6" style="padding-left: 30%; padding-right: 5%;">
                <asp:Button ID="ConfrimBtn" runat="server" Text="Confirm" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="ConfrimBtn_Click" TabIndex="7" />
            </div>

            <div class="form-group has-feedback col-sm-6" style="padding-left: 5%; padding-right: 30%;">
                <asp:Button ID="CancelBtn" runat="server" Text="Cancel" CssClass="btn btn-danger btn-block btn-flat" OnClick="CancelBtn_Click" TabIndex="8" />
            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade " id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-3">
                            <div class="col-sm-9">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                            </div>
                        </div>
                        <h4 class="modal-title">กรุณารอซักครู่กำลังอัพโหลดแบบฟอร์มของท่าน</h4>
                    </div>
                    <%--                <div class="modal-body">

                </div>--%>
                </div>

            </div>
        </div>
    </section>



    <script>

        <%--ฟังก์ชั่นไม่ให้พิมพ์อักษรอื่นยกเว้น เลขและ . --%>
        $(".allownumericwithdecimal").on("keypress", function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        <%--ฟังก์ชั่นเมื่อไม่ focus ในกล่องแล้วในกล่องมีค่า.นำให้เพิ่ม 0 ด้านหน้า หรือหลัง . ไม่มีค่าให้เคลียร์ค่ากล่องนั้นๆ --%>
        $('#CardLimit').focusout(function () {
            var stringofbox1 = $('#CardLimit').val().toString();
            if (stringofbox1.charAt(0) == ".") {
                $('#CardLimit').val("0" + stringofbox1);
                if (stringofbox1.charAt(1) == "") {
                    $('#CardLimit').val("");
                }
            }
        })

        $('#OilMoney').focusout(function () {
            var stringofbox2 = $('#OilMoney').val().toString();
            if (stringofbox2.charAt(0) == ".") {
                $('#OilMoney').val("0" + stringofbox2)
                if (stringofbox2.charAt(1) == "") {
                    $('#OilMoney').val("");
                }
            }
        })

        $('#OilLitre').focusout(function () {
            var stringofbox3 = $('#OilLitre').val().toString();
            if (stringofbox3.charAt(0) == ".") {
                $('#OilLitre').val("0" + stringofbox3)
                if (stringofbox3.charAt(1) == "") {
                    $('#OilLitre').val("");
                }
            }
        })

        <%--ฟังก์ชั่นไม่ให้เลือกวันที่น้อยกว่าวันที่จาก Startdate --%>
        $(function () {
            $("#DateOil").datepicker({
                dateFormat: "dd/mm/yy"
            });
        });


        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'กรุณากรอกข้อมูลให้ครบถ้วน ก่อนกดปุ่มยืนยัน';
                    break;
                case 1:
                    MessageAlert = 'อัพโหลดฟอร์มการขอเติมน้ำมันเป็นเงินสดสำเร็จ';
                    break;
                case 2:
                    MessageAlert = 'อัพโหลดฟอร์ม ผิดพลาด \nกรุณาติดต่อผู้ดูแลระบบ';
                    break;
                case 3:
                    MessageAlert = 'Session หมดเวลาเนื่องจากไม่อยู่ที่หน้าจอนานเกินไป';
                    break;
            }
            alert(MessageAlert);
        }


        <%-- สคริปเช็คนามสกุลไฟล์ --%>
        function checkFileExtension(elem, obj) {
            var filePath = elem.value;

            if (filePath.indexOf('.') == -1) {
                return false;
            }


            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
            var maxFileSize = 4194304;
            var fileUpload = $('#BillUpload');

            validExtensions = ["jpg", "jpeg", "png"];

            for (var i = 0; i < validExtensions.length; i++) {
                if (ext == validExtensions[i]) {
                    if (fileUpload[0].files[0].size < maxFileSize) {
                        return true;
                    }
                    else {
                        alert('ขนาดไฟล์ที่ท่านจะอัพโหลดมีขนาดใหญ่เกินไป\n(ขนาดไฟล์ที่รองรับอยู่ที่ 4Mb)');

                        elem.value = ""; var who2 = elem.cloneNode(false);
                        who2.onchange = elem.onchange;
                        elem.parentNode.replaceChild(who2, elem);
                        $('#BillUpload').trigger('click');
                        return false;
                    }
                }

            }

            alert('นามสกุลไฟล์ที่ท่านเลือกผิด\nกรุณาเลือกนามสกุลไฟล์\n.jpg , .jpeg หรือ .png');

            elem.value = ""; var who2 = elem.cloneNode(false);
            who2.onchange = elem.onchange;
            elem.parentNode.replaceChild(who2, elem);
            $('#BillUpload').trigger('click');
            return false;
        }


        <%--ฟังก์ชั่นเช็คช่องข้อมูลที่ยังไม่กรอกตอนกด confrim --%>
        function checknotchoose() {
            var Plate = $('#CarPlate').prop('selectedIndex');
            var Date = $('#DateOil').val();
            var CardLimit = $('#CardLimit').val();
            var OilMoney = $('#OilMoney').val();
            var OilLitre = $('#OilLitre').val();
            var Filepick = $('#BillUpload').val();
            var Reason = $('#ReasonText').val();

            if (Plate > 0 & $.trim(Date) != "" & $.trim(CardLimit) != "" & $.trim(OilMoney) != "" & $.trim(OilLitre) != "" & $.trim(Reason) != "" & $.trim(Filepick) != "") {
                $('#myModal').modal({ backdrop: 'static', keyboard: false })
                return true;
            }
            else if (Plate <= 0) {
                alert('กรุณากรอกทะเบียนรถที่ท่านต้องการจะยืม');
                $('#CarPlate').select2('open');
                return false;
            }
            else if ($.trim(CardLimit) == "") {
                $('#CardLimit').focus();
                alert('กรุณาระบุวงเงินตาม Fleet Card ของท่านด้วย');
                $('#CardLimit').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#CardLimit').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(OilMoney) == "") {
                $('#OilMoney').focus();
                alert('กรุณาระบุเหตุผลที่ท่านทำการยืมรถ');
                $('#OilMoney').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#OilMoney').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(OilLitre) == "") {
                $('#OilLitre').focus();
                alert('กรุณาระบุเหตุผลที่ท่านทำการยืมรถ');
                $('#OilLitre').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#OilLitre').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(Date) == "") {
                alert('กรุณาเลือกวันที่ที่ตองการเติมน้ำมันเป็นเงินสด');
                $('#DateOil').focus();
                $('#DateOil').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#DateOil').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(Filepick) == "") {
                alert('กรุณาเลือกไฟล์ภาพของใบเสร็จน้ำมันที่ต้องการอัพโหลด');
                $('#BillUpload').trigger('click');
                return false;
            }
            else if ($.trim(Reason) == "") {
                $('#ReasonText').focus();
                alert('กรุณาระบุเหตุผลที่ท่านทำการยืมรถ');
                $('#ReasonText').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#ReasonText').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }

        };

        function HideModal() {
            $('#myModal').modal('hide');
        }

        <%--ฟังก์ชั่น Select2 --%>
        $(function () {
            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '150px'
            });
        });

        <%-- Ajax Plate to Brand --%>

        $("#CarPlate").change(function () {

            var carid = $("#CarPlate").val();
            var JData = { CarID: carid };

            $.ajax({
                type: 'POST',
                url: 'OTBorrowForm.aspx/CarBrandFind',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(JData),
                dataType: 'json',
                success: function (data) {
                    document.getElementById("CarBrand").value = JSON.parse(data.d);
                },
                failure: function (result) {
                    alert(result.d);
                },
                error: function (xhr, status, error) {
                    alert('Exception:Ajax Json Error = ' + error, error);
                }


            });
        });

    </script>

</asp:Content>
