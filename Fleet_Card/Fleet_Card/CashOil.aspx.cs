﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Globalization;
using System.Threading;

namespace Fleet_Card
{
    public partial class CashOil : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        //string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlDataReader readersql;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        string EmpID_Login = string.Empty;
        int Process_ID = 3;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtAllcar = new DataTable(), dtRegion = new DataTable();
                EmpID_Login = Session["EmpID"].ToString();

                if (!IsPostBack)
                {

                    var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
                    objConn.ConnectionString = strConnString;
                    objConn.Open();

                    int CompID = int.Parse(Session["CompCode"].ToString());
                    strSQL = "Select * from Company where CompCode = " + CompID;
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtRegion);

                    if (dtRegion.Rows.Count > 0)
                    {
                        int Region = int.Parse(dtRegion.Rows[0]["Region"].ToString());

                        strSQL = "Select * from View_CarBrand where Region = " + Region;
                        dtAdapter = new SqlDataAdapter(strSQL, objConn);
                        dtAdapter.Fill(dtAllcar);

                        Session["dtAllcar"] = dtAllcar;
                        CarPlate.DataSource = dtAllcar;
                        CarPlate.DataTextField = "LicensePlate_Car";
                        CarPlate.DataValueField = "CarID";
                        CarPlate.DataBind();
                    }

                    CarBrand.Attributes.Add("ReadOnly", "true");
                    DateOil.Attributes.Add("ReadOnly", "true");


                    objConn.Close();

                }
            }
            catch (Exception ex)
            {
                if (objConn != null && objConn.State == ConnectionState.Closed)
                {
                    objConn.Close();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(3);", true);
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void ConfrimBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (CarPlate.SelectedIndex > 0 & DateOil.ToString().Trim() != "" & OilMoney.ToString().Trim() != "" & OilLitre.ToString().Trim() != "" & ReasonText.ToString().Trim() != "" & BillUpload.HasFile)
                {
                    DataTable dtoil = new DataTable();
                    DataRow dr;
                    string docID = DateTime.Now.ToString("yyMMdd") + Session["EmpID"].ToString().Right(4) + DateTime.Now.ToString("HHmmss");
                    string base64String = "";

                    if (BillUpload.PostedFile.ContentLength != 0)
                    {
                        Byte[] b = new byte[BillUpload.PostedFile.ContentLength];
                        BillUpload.PostedFile.InputStream.Read(b, 0, b.Length);
                        base64String = System.Convert.ToBase64String(b, 0, b.Length);
                    }

                    //string extension = System.IO.Path.GetExtension(BillUpload.FileName).ToLower();

                    //string ulpath = "Upload/CashOil_Bill/OB_" + docID + extension;

                    //BillUpload.SaveAs(HttpContext.Current.Server.MapPath(ulpath));

                    dtoil.Columns.Add("ProcessID", typeof(Int16));
                    dtoil.Columns.Add("DocID", typeof(String));
                    dtoil.Columns.Add("EmpID", typeof(String));
                    dtoil.Columns.Add("Plate", typeof(String));
                    dtoil.Columns.Add("Brand", typeof(String));
                    dtoil.Columns.Add("Limit", typeof(float));
                    dtoil.Columns.Add("Oilcash", typeof(float));
                    dtoil.Columns.Add("Oillitre", typeof(float));
                    dtoil.Columns.Add("DateOil", typeof(String));
                    dtoil.Columns.Add("BillingImg", typeof(String));
                    dtoil.Columns.Add("Reason", typeof(String));
                    dtoil.Columns.Add("Region", typeof(String));  //theerayut.s
                    dtoil.Columns.Add("CompCode", typeof(String));  //theerayut.s
                    dtoil.Columns.Add("DepTeam", typeof(String));  //theerayut.s
                    dtoil.Columns.Add("ProvinceArea", typeof(String));  //theerayut.s

                    dr = dtoil.NewRow();
                    dtoil.Rows.Add(dr);

                    dtoil.Rows[0][0] = 3;
                    dtoil.Rows[0][1] = docID;
                    dtoil.Rows[0][2] = Session["EmpID"].ToString();
                    dtoil.Rows[0][3] = CarPlate.SelectedItem.ToString();
                    dtoil.Rows[0][4] = CarBrand.Text;
                    dtoil.Rows[0][5] = CardLimit.Text;
                    dtoil.Rows[0][6] = OilMoney.Text;
                    dtoil.Rows[0][7] = OilLitre.Text;
                    dtoil.Rows[0][8] = DateOil.Text;
                    dtoil.Rows[0][9] = base64String;
                    string HTMLReason = ReasonText.Text.Replace("\r\n", "<br/>");
                    dtoil.Rows[0][10] = HTMLReason;
                    dtoil.Rows[0][11] = Session["Region"].ToString(); //theerayut.s
                    dtoil.Rows[0][12] = Session["CompCode"].ToString(); //theerayut.s
                    dtoil.Rows[0][13] = Session["DepTeam"].ToString(); //theerayut.s
                    dtoil.Rows[0][14] = Session["ProvinceArea"].ToString(); //theerayut.s

                    /////////////////////////////////////////////
                    ///////// รอโค้ดแปลงเป็น XML และนำเข้า SQL ///////
                    string cms_XML = CommonLibrary.Collection.CollectionHelper.ConvertDatatableToXML(dtoil, "XMLCMS_Detail");
                    var Data = Poolfunc.CREATE_CMS_Wolkflow(Session["EmpID"].ToString(), Process_ID, cms_XML);
                    /////////////////////////////////////////////
                    Thread.Sleep(3000);


                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Modal", "HidewaituploadModal();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SendCompleteModal", "ShowSendCompleteModal();", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(1);", true);

                    CarPlate.SelectedIndex = 0;
                    CardLimit.Text = "";
                    OilMoney.Text = "";
                    OilLitre.Text = "";
                    DateOil.Text = "";
                    ReasonText.Text = "";
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(2);", true);
            }

        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            CarPlate.SelectedIndex = 0;
            CardLimit.Text = "";
            OilMoney.Text = "";
            OilLitre.Text = "";
            DateOil.Text = "";
            ReasonText.Text = "";
        }


    }
}









