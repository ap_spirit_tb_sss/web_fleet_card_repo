﻿<%@ Page Title="CMS :: Homepage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="Fleet_Card.MainPage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {

            var currentmenu = document.getElementById("homeID")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link1")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .select {
            color: #9e9e9e !important;
        }

        .option:not(:first-of-type) {
            color: black !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <!-- SlimScroll -->
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <%--//Titleหัวข้อ--%>
    <section class="content-header">
        <h1>หน้าหลัก
            <small>(beta)</small>
        </h1>

        <div class="box" style="margin-top: 20px;">
            <div class="box-header with-border">
                <h3 class="box-title">สถานะการขออนุมัติใช้รถยนต์เกินเวลา</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <img src='Images/Waited_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะรออนุมัติ</label>
                    <img src='Images/Check_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะผ่านการอนุมัติ</label>
                    <img src='Images/Reject_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะไม่ผ่านการอนุมัติ</label>
                    <div id="divTable1" runat="server">
                        <asp:Literal ID="DatatableToHtmlTable1" runat="server"></asp:Literal>
                    </div>
                </div>

            </div>
        </div>

        <div class="box" style="margin-top: 20px;">
            <div class="box-header with-border">
                <h3 class="box-title">สถานะการขออนุมัติจอดรถยนต์นอกสถานที่</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <img src='Images/Waited_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะรออนุมัติ</label>
                    <img src='Images/Check_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะผ่านการอนุมัติ</label>
                    <img src='Images/Reject_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะไม่ผ่านการอนุมัติ</label>
                    <div id="divTable2" runat="server">
                        <asp:Literal ID="DatatableToHtmlTable2" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
        </div>

        <div class="box" style="margin-top: 20px;">
            <div class="box-header with-border">
                <h3 class="box-title">สถานะการขออนุมัติเติมน้ำมันเป็นเงินสด</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <img src='Images/Waited_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะรออนุมัติ</label>
                    <img src='Images/Check_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะผ่านการอนุมัติ</label>
                    <img src='Images/Reject_mini_20px.png' style="padding-left: 6px; padding-right: 3px;" /><label> = สถานะไม่ผ่านการอนุมัติ</label>
                    <div id="div1" runat="server">
                        <asp:Literal ID="DatatableToHtmlTable3" runat="server"></asp:Literal>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <script>
        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>

        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = '!! WARNING !!\nSome request not have data , we already remove it out of table.\nPlease contact adminstrator if you data is missing.';
                    break;
                case 1:
                    MessageAlert = '';
                    break;
                case 2:
                    MessageAlert = '';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }


        <%-- สคริปการนำ Table ด้านบนมาใส่ใน JS ของ Datatable.net --%>
        $(document).ready(function () {
            $('#exampledt1').DataTable({
                'autoWidth': false,
                'scrollY': '50vh',
                'scrollCollapse': true,
                'paging': false,
                'scrollX': true,
                'responsive': false,
                'ordering': false,
            });

            $('#exampledt1').css('display', 'table', 'wrap');
            $('#exampledt1').DataTable().responsive.recalc();
        });

        $(document).ready(function () {
            $('#exampledt2').DataTable({
                'autoWidth': false,
                'scrollY': '50vh',
                'scrollCollapse': true,
                'paging': false,
                'scrollX': true,
                'responsive': false,
                'ordering': false,
            });

            $('#exampledt2').css('display', 'table', 'wrap');
            $('#exampledt2').DataTable().responsive.recalc();
        });

        $(document).ready(function () {
            $('#exampledt3').DataTable({
                'autoWidth': false,
                'scrollY': '50vh',
                'scrollCollapse': true,
                'paging': false,
                'scrollX': true,
                'responsive': false,
                'ordering': false,
            });

            $('#exampledt3').css('display', 'table', 'wrap');
            $('#exampledt3').DataTable().responsive.recalc();
        });
    </script>
</asp:Content>
