﻿<%@ Page Title="CMS :: Overtime Borrow" Language="C#" EnableEventValidation="False" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OTBorrowForm.aspx.cs" Inherits="Fleet_Card.OTBorrowForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainForm")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("OTBorrowID")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link2")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .borderset {
            border-width: 1px;
            border-radius: 7px;
            border-color: grey;
            border-style: solid;
        }

        .select {
            color: #9e9e9e !important;
        }

        .option:not(:first-of-type) {
            color: black !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <link href="dist/calendarextenderdisable/CSS.css" rel="stylesheet" type="text/css" />
    <script src="dist/calendarextenderdisable/Extension.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <%--//Titleหัวข้อ--%>
    <section class="content-header">
        <h1>แบบฟอร์มยืมรถยนต์นอกเวลาทำงาน
            <small>(beta)</small>
        </h1>
        <div style="margin-top: 10px;">
            <div style="margin-bottom: 10px;">
                <asp:Label ID="LabelDescript" runat="server" Text="กรุณากรอกข้อมูล ในช่องต่างๆให้ครบถ้วนและถูกต้อง"></asp:Label>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="form-group has-feedback col-sm-12" style="text-align: center;">
                        <h3>ใบขออนุญาตใช้รถยนต์เกินเวลาของ <%=Session["Company"].ToString() %> </h3>
                    </div>
                    <div class="form-group has-feedback col-sm-12" style="text-align: center;">
                        <label>ประจำวันที่วันที่ <%= DateTime.Now.Date.ToString("dd/MM/yyyy") %></label>
                    </div>
                    <div class="form-group has-feedback col-sm-1" style="text-align: center;">
                    </div>
                    <div class="form-group has-feedback col-sm-11" style="padding-bottom: 30px;">
                        <label class="form-group has-feedback col-sm-12" style="font-weight: normal;">
                            เนื่องด้วยข้าพเจ้า &nbsp&nbsp <b><u><%=Session["Name"].ToString() %>&nbsp&nbsp<%=Session["LastName"].ToString() %></u></b>&nbsp&nbsp&nbsp
                            ตำแหน่ง &nbsp&nbsp&nbsp<b><u><%=Session["EmpPosition"] %></u></b>&nbsp&nbsp&nbsp 
                            ประจำอยู่ที่ &nbsp&nbsp&nbsp <b><u><%=Session["Workplace"] %></u></b>
                        </label>
                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label1" runat="server" Text="มีความประสงค์ขอใช้รถยนต์คันหมายเลขทะเบียน<font color='red'>*</font>"></asp:Label>
                                <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="borderset" runat="server" AppendDataBoundItems="True" TabIndex="1">
                                    <%--OnSelectedIndexChanged="CarPlate_SelectedIndexChanged"--%>
                                    <Items>
                                        <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                    </Items>
                                </asp:DropDownList>
                            </div>
                            <div style="float: left; padding-left: 10px; padding-top: 6px;">
                                <asp:Label ID="Label7" runat="server" Text="ยี่ห้อ"></asp:Label>
                                <asp:TextBox ID="CarBrand" ClientIDMode="Static" CssClass="borderset" runat="server" Style="text-align: center;" />
                            </div>

                        </div>

                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label3" runat="server" Text="ตั้งแต่วันที่<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="StartDate" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" ClientIDMode="Static" onchange="buildEndDate();" TabIndex="2" />
                                <%--                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="StartDate" ClientIDMode="Static" OnClientDateSelectionChanged="saveDate" />--%>
                                <asp:Label ID="Label5" runat="server" Text="เวลา<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="StartTime" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" ClientIDMode="Static" TabIndex="4" />

                            </div>

                            <div style="float: left; padding-left: 5px;">
                                <asp:Label ID="Label4" runat="server" Text="ถึงวันที่<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="EndDate" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" Enabled="false" ClientIDMode="Static" TabIndex="3" />
                                <%--                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" Format="dd/MM/yyyy" TargetControlID="EndDate" ClientIDMode="Static" OnClientDateSelectionChanged="checkDate" />--%>
                                <asp:Label ID="Label6" runat="server" Text="เวลา<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="EndTime" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" Enabled="false" ClientIDMode="Static" TabIndex="5" />
                            </div>
                        </div>

                        <div class="form-group has-feedback col-sm-12" style="float: left;">
                            <asp:Label ID="Label2" runat="server" Text="โดยมีรายละเอียดการนำไปใช้ปฏิบัติงาน ดังนี้<font color='red'>*</font>"></asp:Label>
                        </div>
                        <div class="form-group has-feedback col-sm-12">
                            <asp:TextBox ID="ReasonText" runat="server" CssClass="borderset" ClientIDMode="Static" TextMode="MultiLine" Style="resize: none; width: 80%; height: 300px;" TabIndex="6" />
                        </div>

                    </div>

                </div>
            </div>

            <div class="form-group has-feedback col-sm-6" style="padding-left: 30%; padding-right: 5%;">
                <asp:Button ID="ConfrimBtn" runat="server" Text="Confirm" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="ConfrimBtn_Click" TabIndex="7" />
            </div>

            <div class="form-group has-feedback col-sm-6" style="padding-left: 5%; padding-right: 30%;">
                <asp:Button ID="CancelBtn" runat="server" Text="Cancel" CssClass="btn btn-danger btn-block btn-flat" OnClick="CancelBtn_Click" TabIndex="8" />
            </div>

        </div>

    </section>



    <script>
        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'กรุณากรอกข้อมูลให้ครบถ้วน ก่อนกดปุ่มยืนยัน';
                    break;
                case 1:
                    MessageAlert = 'อัพโหลดฟอร์มการขอยืมรถสำเร็จ';
                    break;
                case 2:
                    MessageAlert = 'อัพโหลดฟอร์ม ! ผิดพลาด !\nกรุณาติดต่อผู้ดูแลระบบ';
                    break;
                case 3:
                    MessageAlert = 'Session หมดเวลาเนื่องจากไม่อยู่ที่หน้าจอนานเกินไป';
                    break;
            }
            alert(MessageAlert);
        }

         <%--ฟังก์ชั่นไม่ให้เลือกวันที่น้อยกว่าวันที่จาก Startdate --%>
        $(function () {
            $("#StartDate").datepicker({
                dateFormat: "dd/mm/yy",
                minDate: "+0d"
            });

            $("#EndDate").datepicker({
                dateFormat: "dd/mm/yy"
            });

            $("#StartTime").timepicker(
            );

            $("#EndTime").timepicker(
            );


            document.getElementById("StartTime").value = "";
            document.getElementById("EndTime").value = "";

        });

        function buildEndDate() {
            if ($("#StartDate").val() != "") {
                document.getElementById("EndDate").disabled = false;
                document.getElementById("EndDate").value = "";
                document.getElementById("EndTime").disabled = false;
                document.getElementById("EndTime").value = "";

                $("#EndDate").datepicker('option', 'minDate', new Date($("#StartDate").datepicker("getDate")));

                $("#EndDate").datepicker('refresh');

            }
            else {
                document.getElementById("EndDate").disabled = true;
                document.getElementById("EndDate").value = "";
                document.getElementById("EndTime").disabled = true;
                document.getElementById("EndTime").value = "";
            }
        }


         <%--ฟังก์ชั่นปิดการทำงานช่อง Enddate กรณี StartDate ไม่กรอก --%>
            //$("#StartDate").change(function () {
            //    if ($("#StartDate").val() != "") {
            //        document.getElementById("EndDate").disabled = false;
            //        document.getElementById("EndDate").value = "";
            //        document.getElementById("EndTime").disabled = false;
            //        document.getElementById("EndTime").value = "";

            //        var dateField = $("#EndDate");
            //        dateField.datepicker('option', 'minDate', new Date($("#StartDate").datepicker("getDate")));
            //        dateField.datepicker('refresh');
            //    }
            //    else {
            //        document.getElementById("EndDate").disabled = true;
            //        document.getElementById("EndDate").value = "";
            //        document.getElementById("EndTime").disabled = true;
            //        document.getElementById("EndTime").value = "";
            //    }
            //});

        <%--ฟังก์ชั่นเช็คช่องข้อมูลที่ยังไม่กรอกตอนกด confrim --%>
        function checknotchoose() {
            var Plate = $('#CarPlate').prop('selectedIndex');
            var StartDate = $('#StartDate').val();
            var EndDate = $('#EndDate').val();
            var StartTime = $('#StartTime').val();
            var EndTime = $('#EndTime').val();
            var Reason = $('#ReasonText').val();

            if (Plate > 0 & $.trim(StartDate) != "" & $.trim(EndDate) != "" & $.trim(StartTime) != "" & $.trim(EndTime) != "" & $.trim(Reason) != "") {
                $('#waitupload').modal({ backdrop: 'static', keyboard: false })
                return true;
            }
            else if (Plate <= 0) {
                alert('กรุณากรอกทะเบียนรถที่ท่านต้องการจะยืม');
                $('#CarPlate').select2('open');
                return false;
            }
            else if ($.trim(StartDate) == "") {
                alert('กรุณาเลือกวันที่เริ่มต้นในการยืมรถ');
                $('#StartDate').focus();
                $('#StartDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(EndDate) == "") {
                alert('กรุณาเลือกวันที่จะทำการคืนรถที่ยืมไป');
                $('#EndDate').focus();
                $('#EndDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#EndDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(StartTime) == "") {
                alert('กรุณาเลือกเวลาที่เริ่มต้นในการยืมรถ');
                $('#StartTime').focus();
                $('#StartTime').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartTime').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(EndTime) == "") {
                alert('กรุณาเลือกเวลาที่จะทำการคืนรถที่ยืมไป');
                $('#EndTime').focus();
                $('#EndTime').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#EndTime').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });

                return false;
            }
            else if ($.trim(Reason) == "") {
                $('#ReasonText').focus();
                alert('กรุณาระบุเหตุผลที่ท่านทำการยืมรถ');
                $('#ReasonText').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#ReasonText').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
        };


        <%--ฟังก์ชั่น Select2 --%>
        $(function () {
            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '150px'
            });
        });

        <%-- Ajax Plate to Brand --%>

        $("#CarPlate").change(function () {

            var carid = $("#CarPlate").val();
            var JData = { CarID: carid };

            $.ajax({
                type: 'POST',
                url: 'OTBorrowForm.aspx/CarBrandFind',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(JData),
                dataType: 'json',
                success: function (data) {
                    document.getElementById("CarBrand").value = JSON.parse(data.d);
                },
                failure: function (result) {
                    alert(result.d);
                },
                error: function (xhr, status, error) {
                    alert('Exception:Ajax Json Error = ' + error, error);
                }


            });
        });

        //function Onsuccess(data) {
        //    document.getElementById("CarBrand").value = data;
        //};

    </script>


</asp:Content>
