﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveOTBorrow.aspx.cs" Inherits="Fleet_Card.ApproveOTBorrow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainApprove")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("AppOTBID")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link5")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .select {
            color: #9e9e9e !important;
        }

        .option:not(:first-of-type) {
            color: black !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>

    <!-- SlimScroll -->
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="bower_components/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.js"></script>

    <%--//Titleหัวข้อ--%>
    <section class="content-header">
        <h1>การอนุมัติใช้รถยนต์เกินเวลา
            <small>(beta)</small>
        </h1>

        <div class="box" style="margin-top: 20px;">
            <div class="box-body">

                <div id="divTable1" runat="server" class="box-body col-lg-12">
                    <asp:Literal ID="DatatableToHtmlTable1" runat="server"></asp:Literal>
                </div>

            </div>
        </div>

        <div class="form-group has-feedback col-sm-6" style="padding-left: 30%; padding-right: 5%;">
            <asp:Button ID="SubmitBtn" runat="server" ClientIDMode="Static" Text="Approve" CssClass="btn btn-success btn-block btn-flat" OnClick="SubmitBtn_Click" TabIndex="7" />
        </div>
        <asp:HiddenField ID="SubmitDocid" ClientIDMode="Static" runat="server" />

        <div class="form-group has-feedback col-sm-6" style="padding-left: 5%; padding-right: 30%;">
            <asp:Button ID="RejectBtn" runat="server" ClientIDMode="Static" Text="Reject" CssClass="btn btn-danger btn-block btn-flat" OnClick="RejectBtn_Click" TabIndex="8" />
        </div>
        <asp:HiddenField ID="RejectDocid" ClientIDMode="Static" runat="server" />

        <asp:HiddenField ID="Firstloadchecker" ClientIDMode="Static" runat="server" />

       
    </section>

    <script>
        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>

        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'ระบบได้ทำการอนุมัติเอกสารที่ท่านเลือกสำเร็จ';
                    break;
                case 1:
                    MessageAlert = 'ระบบได้ทำการ"ไม่"อนุมัติเอกสารที่ท่านเลือกสำเร็จ';
                    break;
                case 2:
                    MessageAlert = '';
                    break;
                case 3:
                    MessageAlert = 'Session หมดเวลาเนื่องจากไม่อยู่ที่หน้าจอนานเกินไป';
                    break;
            }
            alert(MessageAlert)
            //if (!alert(MessageAlert)) {
            //    window.location.reload();
            //}

<%--            alert(MessageAlert);
            var loc = window.location.origin + window.location.pathname + '?token=<%= Session["Token"].ToString()%>';
            window.location = loc;--%>

        }



        <%-- สคริปการนำ Table ด้านบนมาใส่ใน JS ของ Datatable.net --%>
        $(document).ready(function () {
            var otable = $('#exampledt1').DataTable({
                'autoWidth': false,
                'scrollY': '50vh',
                'scrollCollapse': true,
                'paging': false,
                'scrollX': true,
                'responsive': false,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                'ordering': false,
                'order': false
            });

            $('#exampledt1').css('display', 'table', 'wrap');
            $('#exampledt1').DataTable().responsive.recalc();

            <%-- สคริปการนำเลขเอกสารจากแถวที่ถูกเลือกไปเก็บไว้ใน hidden feild--%>
            $('#SubmitBtn').on('click', function () {


                var rowselect_array = otable.column(0).checkboxes.selected();
                var select_checker = otable.column().checkboxes.selected()[0];
                var docid = "";

                if (typeof select_checker !== "undefined") {
                    $(rowselect_array).each(function (index, rowid) {
                        var datastring = otable.rows(rowid).data();
                        $.each(datastring, function (subindex, rowdata) {
                            docid += rowdata[1] + "/";
                        });
                    });
                    var showdocid = docid.split("/").join("\n")
                    var c = confirm("คุณต้องการอนุมัติเอกสาร หมายเลข\n" + showdocid)
                    if (c) {
                        $('#myModal').modal({ backdrop: 'static', keyboard: false })
                        $('#SubmitDocid').val(docid);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    alert("กรุณาเลือกเอกสารที่ต้องการอนุมัติก่อนทำการกดปุ่มอนุมัติ")
                    return false;
                }

            });

            $('#RejectBtn').on('click', function () {


                var rowselect_array = otable.column(0).checkboxes.selected();
                var select_checker = otable.column().checkboxes.selected()[0];
                var docid = "";
                if (typeof select_checker !== "undefined") {
                    $(rowselect_array).each(function (index, rowid) {
                        var datastring = otable.rows(rowid).data();
                        $.each(datastring, function (subindex, rowdata) {
                            docid += rowdata[1] + "/";
                        });
                    });
                    var showdocid = docid.split("/").join("\n")
                    var c = confirm("คุณต้องการ-'ไม่'-อนุมัติเอกสาร หมายเลข\n" + showdocid)
                    if (c) {
                        $('#myModal').modal({ backdrop: 'static', keyboard: false })
                        $('#RejectDocid').val(docid);
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    alert("กรุณาเลือกเอกสารที่ต้องการอนุมัติก่อนทำการกดปุ่มไม่อนุมัติ")
                    return false;
                }
            });
        });



    </script>

</asp:Content>
