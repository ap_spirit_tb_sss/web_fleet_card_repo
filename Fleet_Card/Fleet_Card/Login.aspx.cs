﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Fleet_Card
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            Session.RemoveAll();
        }

        protected void SigninBtn_Click1(object sender, EventArgs e)
        {
            //username.Value = "11014928";
            //password.Value = "Chn#1907";

            //username.Value = "11034774";
            //password.Value = "Singke@w1234";
            //GET_System_UserInfo("11007820");

            try
            {
                //DataTable ADtable = LoginThaiBev.LogIn.Get_Login_UserInfo(username.Value.ToString(), password.Value.ToString());
                DataTable ADtable = Get_Login_UserInfo(username.Value.ToString(),password.Value.ToString());
                DataTable EmpID_Info = LoginThaiBev.EmpInfo.GetEmployeeINFO(username.Value.ToString());
                String[] TokenAry = new string[] { };

                //if (ADtable.Rows.Count > 0)
                if (ADtable.Rows.Count > 0)
                {
                    Session["EmpPhoto"] = LoginThaiBev.PhotoProfile.Get_Photo_UserProfile(username.Value.ToString());
                    Session["ADTable"] = ADtable;

                    //----------------------------- IT AD Login IUserInfo ---------------------------------------------------------------------------------
                    //TokenAry = LoginThaiBev.LogIn.PostLogin(username.Value.ToString(), password.Value.ToString());
                    //Session["Token"] = TokenAry[4].ToString();
                    //Session["EmpID"] = ADtable.Rows[0][1].ToString();
                    //Session["EmpEmail"] = ADtable.Rows[0][2].ToString();
                    //Session["Name"] = ADtable.Rows[0][5].ToString();
                    //Session["LastName"] = ADtable.Rows[0][6].ToString();
                    //Session["EmpFullName"] = ADtable.Rows[0][5].ToString() + "  " + ADtable.Rows[0][6].ToString();
                    //Session["ComCode"] = ADtable.Rows[0][8].ToString();
                    //Session["Company"] = ADtable.Rows[0][9].ToString();
                    //Session["Org"] = ADtable.Rows[0][10].ToString();
                    //Session["EmpPosition"] = ADtable.Rows[0][11].ToString();
                    //Session["Workplace"] = ADtable.Rows[0][12].ToString();
                    //Session["FristTime"] = 1;
                    //Session["Region"] = Poolfunc.Find_Region(int.Parse(ADtable.Rows[0][8].ToString()));

                    //----------------------------- On the CMS system Login IUserInfo ---------------------------------------------------------------------------------
                    //TokenAry = LoginThaiBev.LogIn.PostLogin(username.Value.ToString(), password.Value.ToString());
                    Session["Token"] = "tester_ss";
                    Session["EmpID"] = ADtable.Rows[0][1].ToString();
                    Session["EmpEmail"] = ""; //ADtable.Rows[0][2].ToString();
                    Session["Name"] = ADtable.Rows[0][11].ToString();
                    Session["LastName"] = ADtable.Rows[0][12].ToString();
                    Session["EmpFullName"] = ADtable.Rows[0][11].ToString() + "  " + ADtable.Rows[0][12].ToString();
                    Session["CompCode"] = ADtable.Rows[0][5].ToString();
                    Session["Company"] = ADtable.Rows[0][13].ToString();
                    Session["Org"] = "-"; //ADtable.Rows[0][10].ToString();
                    Session["EmpPosition"] = ADtable.Rows[0][2].ToString();
                    Session["Workplace"] = ADtable.Rows[0][4].ToString();
                    Session["FristTime"] = 1;
                    Session["Region"] = Poolfunc.Find_Region(int.Parse(ADtable.Rows[0][0].ToString()));

                    //GET_System_UserInfo(Session["EmpID"].ToString());
                    GET_System_UserInfo(username.Value.ToString());                    

                    Session["All_Request"] = 0;  //theerayut.s : 29/08/2018
                    Session["All_Approve"] = 0;  //theerayut.s : 29/08/2018
                    Session["All_Reject"] = 0;   //theerayut.s : 29/08/2018
                    //Response.Redirect("MainPage.aspx?token=" + TokenAry[4].ToString(),false);
                    Response.Redirect("MainPage.aspx?token=" + "Test", false); 
                }
                else
                {
                    username.Value = "";
                    password.Value = "";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
                }

            }
            catch (Exception ex)
            {
                username.Value = "";
                password.Value = "";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
            }
        }

        public void GET_System_UserInfo(string EmpID)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            SqlDataAdapter dtAdapter;
            string strSQL;
            SqlDataReader readersql;
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            DataTable dt_Sys_UserInfo = new DataTable();

            try
            {
                objConn.ConnectionString = connStr;
                objConn.Open();

                int CompID = int.Parse(Session["CompCode"].ToString());
                strSQL = "SELECT top 1 * FROM [DBCMS].[dbo].[View_Get_SysUser_Info] where [EmpID] = " + EmpID;
                dtAdapter = new SqlDataAdapter(strSQL, objConn);
                dtAdapter.Fill(dt_Sys_UserInfo);

                if (dt_Sys_UserInfo.Rows.Count > 0)
                {
                    Session["Region"] = dt_Sys_UserInfo.Rows[0]["Region"].ToString();
                    Session["CompCode"] = dt_Sys_UserInfo.Rows[0]["CompCode"].ToString();
                    Session["DepTeam"] = dt_Sys_UserInfo.Rows[0]["Department"].ToString();
                    Session["ProvinceArea"] = dt_Sys_UserInfo.Rows[0]["Dep_Province"].ToString();
                    Session["CarNo"] = dt_Sys_UserInfo.Rows[0]["LicensePlate_Car"].ToString();
                    Session["CardNo"] = dt_Sys_UserInfo.Rows[0]["FleetCardNo"].ToString();
                    Session["DeviceToken"] = dt_Sys_UserInfo.Rows[0]["DeviceToken"].ToString();
                }

                dtAdapter.Dispose();
                objConn.Close();
                objConn.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public DataTable Get_Login_UserInfo(string EmpID,string Password)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            SqlDataAdapter dtAdapter;
            string strSQL;
            SqlDataReader readersql;
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            DataTable dt_UserInfo = new DataTable();

            try
            {
                objConn.ConnectionString = connStr;
                objConn.Open();

                //int CompID = int.Parse(Session["CompCode"].ToString());
                strSQL = "SELECT top 1 * FROM [DBCMS].[dbo].[View_Get_SysUser_Info] where [EmpID] = " + EmpID;
                dtAdapter = new SqlDataAdapter(strSQL, objConn);
                dtAdapter.Fill(dt_UserInfo);

                if (dt_UserInfo.Rows.Count > 0)
                {
                    if (Password != dt_UserInfo.Rows[0]["PassWord"].ToString())
                    {
                        dt_UserInfo.Clear();
                    }

                }

                dtAdapter.Dispose();
                objConn.Close();
                objConn.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }

            return dt_UserInfo;
        }

    }
}