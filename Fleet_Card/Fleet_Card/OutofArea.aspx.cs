﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

namespace Fleet_Card
{
    public partial class OutofArea : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlDataReader readersql;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        string EmpID_Login = string.Empty;
        int Process_ID = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //DataTable dtAllcar = new DataTable(), dtRegion = new DataTable();
                EmpID_Login = Session["EmpID"].ToString();

                if (!IsPostBack)
                {
                    DataTable dtProvince = new DataTable(), dtProToAmp = new DataTable(), dtProToDist = new DataTable(), dtRegion = new DataTable(), dtAllcar = new DataTable();

                    var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
                    objConn.ConnectionString = strConnString;
                    objConn.Open();

                    int CompID = int.Parse(Session["CompCode"].ToString());
                    strSQL = "Select * from Company where CompCode = " + CompID;
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtRegion);

                    strSQL = "Select * from Province";
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtProvince);

                    strSQL = "Select * from View_ProvinceToAmp";
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtProToAmp);

                    strSQL = "Select * from View_ProvinceToDis";
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtProToDist);



                    if (dtRegion.Rows.Count > 0)
                    {
                        int Region = int.Parse(dtRegion.Rows[0]["Region"].ToString());

                        strSQL = "Select * from View_CarBrand where Region = " + Region;
                        dtAdapter = new SqlDataAdapter(strSQL, objConn);
                        dtAdapter.Fill(dtAllcar);

                        Session["dtAllcar"] = dtAllcar;
                        CarPlate.DataSource = dtAllcar;
                        CarPlate.DataTextField = "LicensePlate_Car";
                        CarPlate.DataValueField = "CarID";
                        CarPlate.DataBind();
                    }

                    Session["dtProvince"] = dtProvince;
                    Session["dtProToAmp"] = dtProToAmp;
                    Session["dtProToDist"] = dtProToDist;

                    SelectProvince.DataSource = dtProvince;
                    SelectProvince.DataTextField = "PROVINCE_NAME";
                    SelectProvince.DataValueField = "PROVINCE_ID";
                    SelectProvince.DataBind();

                    CarBrand.Attributes.Add("ReadOnly", "true");
                    StartDate.Attributes.Add("ReadOnly", "true");
                    EndDate.Attributes.Add("ReadOnly", "true");
                    StartTime.Attributes.Add("ReadOnly", "true");
                    EndTime.Attributes.Add("ReadOnly", "true");
                    objConn.Close();
                }
            }
            catch (Exception ex)
            {
                if (objConn != null && objConn.State == ConnectionState.Closed)
                {
                    objConn.Close();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(3);", true);
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void ConfrimBtn_Click(object sender, EventArgs e)
        {
            DataTable dtOutOfArea = new DataTable();
            DataRow dr;
            try
            {
                if (CarPlate.SelectedIndex > 0 & SelectProvince.SelectedIndex > 0 & HiddenAmphur.Value != "0" & HiddenDistrict.Value != "0" & HomeNumber.Text != "" & StartDate.Text != "" & EndDate.Text != "" & StartTime.Text != "" & EndTime.Text != "" & WorktimeEnd.Text != "" & WorktimeStart.Text != "")
                {
                    dtOutOfArea.Columns.Add("ProcessID", typeof(Int16));
                    dtOutOfArea.Columns.Add("DocID", typeof(String));
                    dtOutOfArea.Columns.Add("EmpID", typeof(String));
                    dtOutOfArea.Columns.Add("WorkStart", typeof(String));
                    dtOutOfArea.Columns.Add("WorkEnd", typeof(String));
                    dtOutOfArea.Columns.Add("Plate", typeof(String));
                    dtOutOfArea.Columns.Add("Brand", typeof(String));
                    dtOutOfArea.Columns.Add("StartDate", typeof(String));
                    dtOutOfArea.Columns.Add("EndDate", typeof(String));
                    dtOutOfArea.Columns.Add("StartTime", typeof(String));
                    dtOutOfArea.Columns.Add("EndTime", typeof(String));
                    dtOutOfArea.Columns.Add("HomeNumber", typeof(String));
                    dtOutOfArea.Columns.Add("VilNumber", typeof(String));
                    dtOutOfArea.Columns.Add("AlleyName", typeof(String));
                    dtOutOfArea.Columns.Add("RoadName", typeof(String));
                    dtOutOfArea.Columns.Add("Province", typeof(String));
                    dtOutOfArea.Columns.Add("Amphur", typeof(String));
                    dtOutOfArea.Columns.Add("District", typeof(String));
                    dtOutOfArea.Columns.Add("Region", typeof(String));  //theerayut.s
                    dtOutOfArea.Columns.Add("CompCode", typeof(String));  //theerayut.s
                    dtOutOfArea.Columns.Add("DepTeam", typeof(String));  //theerayut.s
                    dtOutOfArea.Columns.Add("ProvinceArea", typeof(String));  //theerayut.s

                    dr = dtOutOfArea.NewRow();
                    dtOutOfArea.Rows.Add(dr);

                    dtOutOfArea.Rows[0][0] = 1;
                    dtOutOfArea.Rows[0][1] = DateTime.Now.ToString("yyMMdd") + Session["EmpID"].ToString().Right(4) + DateTime.Now.ToString("hhmmss");
                    dtOutOfArea.Rows[0][2] = Session["EmpID"].ToString();
                    dtOutOfArea.Rows[0][3] = DateTime.Parse(WorktimeStart.Text).ToString("MM/dd/yyyy");
                    dtOutOfArea.Rows[0][4] = DateTime.Parse(WorktimeEnd.Text).ToString("MM/dd/yyyy");
                    dtOutOfArea.Rows[0][5] = CarPlate.SelectedItem.ToString();
                    dtOutOfArea.Rows[0][6] = CarBrand.Text;
                    dtOutOfArea.Rows[0][7] = DateTime.ParseExact(StartDate.Text, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US")).ToString("MM/dd/yyyy"); //DateTime.Parse(StartDate.Text).ToString("MM/dd/yyyy");
                    dtOutOfArea.Rows[0][8] = DateTime.ParseExact(EndDate.Text, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US")).ToString("MM/dd/yyyy");  //DateTime.Parse(EndDate.Text).ToString("MM/dd/yyyy");
                    dtOutOfArea.Rows[0][9] = StartTime.Text;
                    dtOutOfArea.Rows[0][10] = EndTime.Text;
                    dtOutOfArea.Rows[0][11] = HomeNumber.Text;
                    dtOutOfArea.Rows[0][12] = VilNumber.Text;
                    dtOutOfArea.Rows[0][13] = AlleyName.Text;
                    dtOutOfArea.Rows[0][14] = RoadName.Text;
                    dtOutOfArea.Rows[0][15] = SelectProvince.SelectedItem;
                    dtOutOfArea.Rows[0][16] = HiddenAmphur.Value.ToString().Trim();
                    dtOutOfArea.Rows[0][17] = HiddenDistrict.Value.ToString().Trim();
                    dtOutOfArea.Rows[0][18] = Session["Region"].ToString(); //theerayut.s
                    dtOutOfArea.Rows[0][19] = Session["CompCode"].ToString(); //theerayut.s
                    dtOutOfArea.Rows[0][20] = Session["DepTeam"].ToString(); //theerayut.s
                    dtOutOfArea.Rows[0][21] = Session["ProvinceArea"].ToString(); //theerayut.s

                    /////////////////////////////////////////////
                    ///////// รอโค้ดแปลงเป็น XML และนำเข้า SQL ///////
                    string cms_XML = CommonLibrary.Collection.CollectionHelper.ConvertDatatableToXML(dtOutOfArea, "XMLCMS_Detail");
                    var Data = Poolfunc.CREATE_CMS_Wolkflow(Session["EmpID"].ToString(), Process_ID, cms_XML);
                    /////////////////////////////////////////////
                    Thread.Sleep(3000);

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Modal", "HidewaituploadModal();", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SendCompleteModal", "ShowSendCompleteModal();", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(1);", true);

                    CarPlate.SelectedIndex = 0;
                    StartDate.Text = "";
                    EndDate.Text = "";
                    EndDate.Enabled = false;
                    StartTime.Text = "";
                    EndTime.Text = "";
                    WorktimeEnd.Text = "";
                    WorktimeStart.Text = "";
                    SelectProvince.SelectedIndex = 0;
                    SelectSubDistrict.SelectedIndex = 0;
                    SelectSubDistrict.Enabled = false;
                    SelectDistrict.SelectedIndex = 0;
                    SelectDistrict.Enabled = false;
                    HomeNumber.Text = "";
                    VilNumber.Text = "";
                    AlleyName.Text = "";
                    RoadName.Text = "";
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(2);", true);
            }
        }
        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            CarPlate.SelectedIndex = 0;
            StartDate.Text = "";
            EndDate.Text = "";
            EndDate.Enabled = false;
            StartTime.Text = "";
            EndTime.Text = "";
            WorktimeEnd.Text = "";
            WorktimeStart.Text = "";
            SelectProvince.SelectedIndex = 0;
            SelectSubDistrict.SelectedIndex = 0;
            SelectSubDistrict.Enabled = false;
            SelectDistrict.SelectedIndex = 0;
            SelectDistrict.Enabled = false;
            HomeNumber.Text = "";
            VilNumber.Text = "";
            AlleyName.Text = "";
            RoadName.Text = "";
        }


        [WebMethod(EnableSession = true)]
        public static string FilterAmphurandDistrict(string ProVal, string AmpVal, string Type)
        {
            int i = 0;
            string data = "";

            if (Type == "1")
            {
                List<ProvinceObj> AmpList = new List<ProvinceObj>();

                DataTable dtProToAmp = (DataTable)HttpContext.Current.Session["dtProToAmp"];
                DataView dvProToAmp = new DataView(dtProToAmp);

                if (ProVal != null)
                {
                    if (ProVal != "0")
                    {


                        dvProToAmp.RowFilter = "PROVINCE_ID = " + ProVal;

                        for (i = 0; i <= dvProToAmp.ToTable().Rows.Count; i++)
                        {
                            ProvinceObj details = new ProvinceObj();
                            if (i == 0)
                            {
                                details.id = "0";
                                details.text = "Place_Holder";

                                AmpList.Add(details);
                            }
                            else
                            {
                                details.id = dvProToAmp.ToTable().Rows[i - 1][3].ToString();
                                details.text = dvProToAmp.ToTable().Rows[i - 1][1].ToString();
                                AmpList.Add(details);
                            }
                        }

                        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        data = jsonSerializer.Serialize(AmpList);
                        return data;

                    }
                }
            }
            else
            {
                List<ProvinceObj> DistList = new List<ProvinceObj>();

                DataTable dtProToDist = (DataTable)HttpContext.Current.Session["dtProToDist"];
                DataView dvProToDist = new DataView(dtProToDist);

                if (ProVal != null)
                {
                    if (ProVal != "0")
                    {
                        if (AmpVal != "0")
                        {

                            dvProToDist.RowFilter = "PROVINCE_ID = " + ProVal + " and AMPHUR_ID = " + AmpVal;

                            for (i = 0; i <= dvProToDist.ToTable().Rows.Count; i++)
                            {
                                ProvinceObj details = new ProvinceObj();
                                if (i == 0)
                                {
                                    details.id = "0";
                                    details.text = "Place_Holder";

                                    DistList.Add(details);
                                }
                                else
                                {
                                    details.id = dvProToDist.ToTable().Rows[i - 1][5].ToString();
                                    details.text = dvProToDist.ToTable().Rows[i - 1][2].ToString();

                                    DistList.Add(details);
                                }
                            }


                            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                            data = jsonSerializer.Serialize(DistList);
                            return data;
                        }

                    }
                }

            }
            return data;
        }

        [WebMethod(EnableSession = true)]
        public static string CarBrandFind(string CarID)
        {
            DataTable dtAllcar = (DataTable)HttpContext.Current.Session["dtAllcar"];
            DataView dvAllcar = new DataView(dtAllcar);
            string Brand = "";

            if (CarID != null)
            {
                if (CarID != "0")
                {
                    dvAllcar.RowFilter = "CarID = " + CarID;
                    Brand = dvAllcar.ToTable().Rows[0]["CarBrandName"].ToString();
                }
            }

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string data = jsonSerializer.Serialize(Brand);
            return data;

        }

        public class ProvinceObj
        {
            public string id { get; set; }
            public string text { get; set; }
        }


    }
}