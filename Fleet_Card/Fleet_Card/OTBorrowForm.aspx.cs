﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

namespace Fleet_Card
{
    public partial class OTBorrowForm : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlDataReader readersql;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        string EmpID_Login = string.Empty;
        int Process_ID = 2;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable dtAllcar = new DataTable(), dtRegion = new DataTable();
                EmpID_Login = Session["EmpID"].ToString();

                if (!IsPostBack)
                {

                    var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
                    objConn.ConnectionString = strConnString;
                    objConn.Open();

                    int CompID = int.Parse(Session["CompCode"].ToString());
                    strSQL = "Select * from Company where CompCode = " + CompID;
                    dtAdapter = new SqlDataAdapter(strSQL, objConn);
                    dtAdapter.Fill(dtRegion);

                    if (dtRegion.Rows.Count > 0)
                    {
                        int Region = int.Parse(dtRegion.Rows[0]["Region"].ToString());

                        strSQL = "Select * from View_CarBrand where Region = " + Region;
                        dtAdapter = new SqlDataAdapter(strSQL, objConn);
                        dtAdapter.Fill(dtAllcar);

                        Session["dtAllcar"] = dtAllcar;
                        CarPlate.DataSource = dtAllcar;
                        CarPlate.DataTextField = "LicensePlate_Car";
                        CarPlate.DataValueField = "CarID";
                        CarPlate.DataBind();
                    }


                    CarBrand.Attributes.Add("ReadOnly", "true");
                    StartDate.Attributes.Add("ReadOnly", "true");
                    EndDate.Attributes.Add("ReadOnly", "true");
                    StartTime.Attributes.Add("ReadOnly", "true");
                    EndTime.Attributes.Add("ReadOnly", "true");

                    objConn.Close();


                }
            }
            catch (Exception ex)
            {
                if (objConn != null && objConn.State == ConnectionState.Closed)
                {
                    objConn.Close();
                }
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(3);", true);
                Response.Redirect("~/Login.aspx");
            }
        }

        //protected void CarPlate_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtAllcar = (DataTable)Session["dtAllcar"];
        //    DataView dvAllcar = new DataView(dtAllcar);

        //    int valueplate = int.Parse(CarPlate.SelectedValue.ToString());

        //    if (valueplate > 0)
        //    {
        //        dvAllcar.RowFilter = "CarID = " + valueplate + "";
        //        string Brand = dvAllcar.ToTable().Columns["CarBrandName"].ToString();
        //        CarBrand.Text = Brand;
        //    }
        //    else
        //    {
        //        CarBrand.Text = "";
        //    }

        //}

        protected void ConfrimBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (CarPlate.SelectedIndex != 0 && CarBrand.Text != "" && StartDate.Text != "" && EndDate.Text != "" && StartTime.Text != "" && EndTime.Text != "" && ReasonText.Text != "")
                {
                    DataTable dtBorrow = new DataTable();
                    DataRow dr;

                    dtBorrow.Columns.Add("ProcessID", typeof(Int16));
                    dtBorrow.Columns.Add("DocID", typeof(String));
                    dtBorrow.Columns.Add("EmpID", typeof(String));
                    dtBorrow.Columns.Add("Plate", typeof(String));
                    dtBorrow.Columns.Add("Brand", typeof(String));
                    dtBorrow.Columns.Add("StartDate", typeof(String));
                    dtBorrow.Columns.Add("EndDate", typeof(String));
                    dtBorrow.Columns.Add("StartTime", typeof(String));
                    dtBorrow.Columns.Add("EndTime", typeof(String));
                    dtBorrow.Columns.Add("Reason", typeof(String));
                    dtBorrow.Columns.Add("Region", typeof(String));  //theerayut.s
                    dtBorrow.Columns.Add("CompCode", typeof(String));  //theerayut.s
                    dtBorrow.Columns.Add("DepTeam", typeof(String));  //theerayut.s
                    dtBorrow.Columns.Add("ProvinceArea", typeof(String));  //theerayut.s

                    dr = dtBorrow.NewRow();
                    dtBorrow.Rows.Add(dr);

                    dtBorrow.Rows[0][0] = Process_ID;
                    dtBorrow.Rows[0][1] = DateTime.Now.ToString("yyMMdd") + Session["EmpID"].ToString().Right(4) + DateTime.Now.ToString("HHmmss");
                    dtBorrow.Rows[0][2] = Session["EmpID"].ToString();
                    dtBorrow.Rows[0][3] = CarPlate.SelectedItem.ToString();
                    dtBorrow.Rows[0][4] = CarBrand.Text;
                    dtBorrow.Rows[0][5] = DateTime.ParseExact(StartDate.Text, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US")).ToString("MM/dd/yyyy"); //DateTime.Parse(StartDate.Text).ToString("MM/dd/yyyy");
                    dtBorrow.Rows[0][6] = DateTime.ParseExact(EndDate.Text, "dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US")).ToString("MM/dd/yyyy");  /*DateTime.Parse(EndDate.Text).ToString("MM/dd/yyyy");*/
                    dtBorrow.Rows[0][7] = StartTime.Text;
                    dtBorrow.Rows[0][8] = EndTime.Text;
                    string HTMLReason = ReasonText.Text.Replace("\r\n", "<br/>");
                    dtBorrow.Rows[0][9] = HTMLReason;
                    dtBorrow.Rows[0][10] = Session["Region"].ToString(); //theerayut.s
                    dtBorrow.Rows[0][11] = Session["CompCode"].ToString(); //theerayut.s
                    dtBorrow.Rows[0][12] = Session["DepTeam"].ToString(); //theerayut.s
                    dtBorrow.Rows[0][13] = Session["ProvinceArea"].ToString(); //theerayut.s

                    /////////////////////////////////////////////
                    ///////// รอโค้ดแปลงเป็น XML และนำเข้า SQL ///////
                    string cms_XML = CommonLibrary.Collection.CollectionHelper.ConvertDatatableToXML(dtBorrow, "XMLCMS_Detail");
                    var Data = Poolfunc.CREATE_CMS_Wolkflow(EmpID_Login, Process_ID, cms_XML);
                    /////////////////////////////////////////////
                    Thread.Sleep(3000);

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Modal", "HidewaituploadModal();", true);

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "SendCompleteModal", "ShowSendCompleteModal();", true);
                    //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(1);", true);

                    CarPlate.SelectedIndex = 0;
                    StartDate.Text = "";
                    EndDate.Text = "";
                    EndDate.Enabled = false;
                    StartTime.Text = "";
                    EndTime.Text = "";
                    ReasonText.Text = "";

                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
                }
            }
            catch (Exception ex)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(2);", true);
            }
        }

        protected void CancelBtn_Click(object sender, EventArgs e)
        {
            CarPlate.SelectedIndex = 0;
            StartDate.Text = "";
            EndDate.Text = "";
            EndDate.Enabled = false;
            StartTime.Text = "";
            EndTime.Text = "";
            ReasonText.Text = "";
        }


        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                CarPlate.SelectedIndex = 0;
                CarBrand.Text = "";
                StartDate.Text = "";
                EndDate.Text = "";
                EndDate.Enabled = false;
                StartTime.Text = "";
                EndTime.Text = "";
                ReasonText.Text = "";
            }
        }


        public Tuple<string, string, int> CREATE_CMS_Wolkflow(string EmpId_Login, int Process_ID, string cmsDetailxml)
        {
            string Doc_id = "";
            string EmpID_Current = "";
            int dsresult = 0;
            DataSet ds = new DataSet();
            string strSql = @"[dbo].[CREATE_CMS_Wolkflow]";

            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand dCmd = new SqlCommand(strSql, conn);
            dCmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dCmd.Parameters.Add("@Process_ID", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = Process_ID;
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@UserID_Login", SqlDbType.Int);
                dCmd.Parameters[1].Value = EmpId_Login;
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@WF_Detail", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = cmsDetailxml;
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                //dCmd.Parameters.Add("@Current_EmpID", SqlDbType.NVarChar); //Return EmpID who the next current Approver step
                //dCmd.Parameters[3].Value = EmpID_Current;
                //dCmd.Parameters[3].Direction = ParameterDirection.Output;

                //dCmd.Parameters.Add("@Doc_ID", SqlDbType.NVarChar);
                //dCmd.Parameters[4].Value = Doc_id;
                //dCmd.Parameters[4].Direction = ParameterDirection.Output;

                //dCmd.Parameters.Add("@Result", SqlDbType.Int);
                //dCmd.Parameters[5].Value = dsresult;
                //dCmd.Parameters[5].Direction = ParameterDirection.Output;

                SqlParameter EmpIDCurrent = new SqlParameter("@Current_EmpID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(EmpIDCurrent);
                SqlParameter Docid = new SqlParameter("@Doc_ID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(Docid);
                SqlParameter Output = new SqlParameter("@Result", SqlDbType.Int, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(Output);


                dCmd.ExecuteNonQuery();

                // read output value from @Result
                //dsresult = Convert.ToInt32(dCmd.Parameters["@Result"].Value);

                EmpID_Login = EmpIDCurrent.Value.ToString();
                Doc_id = Docid.Value.ToString();
                dsresult = Convert.ToInt32(Output.Value.ToString());

                return new Tuple<string, string, int>(EmpID_Current, Doc_id, dsresult);

                //return Doc_id;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }

        [WebMethod(EnableSession = true)]
        public static string CarBrandFind(string CarID)
        {
            DataTable dtAllcar = (DataTable)HttpContext.Current.Session["dtAllcar"];
            DataView dvAllcar = new DataView(dtAllcar);
            string Brand = "";

            if (CarID != null)
            {
                if (CarID != "0")
                {
                    dvAllcar.RowFilter = "CarID = " + CarID;
                    Brand = dvAllcar.ToTable().Rows[0]["CarBrandName"].ToString();
                }
            }

            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            string data = jsonSerializer.Serialize(Brand);
            return data;

        }

    }

    static class Extensions
    {
        public static string Right(this string value, int length)
        {
            return value.Substring(value.Length - length);
        }
    }
}