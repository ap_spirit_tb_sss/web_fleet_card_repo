﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Fleet_Card
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Confrim_Click(object sender, EventArgs e)
        {

            try
            {
                var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
                objConn.ConnectionString = strConnString;
                objConn.Open();

                string Empid = username.Value.ToString();
                string email = Email.Value.ToString();

                DataTable dtEmpFound = new DataTable();

                strSQL = "Select top 1 * from Employee where EmpID = " + Empid;
                dtAdapter = new SqlDataAdapter(strSQL, objConn);
                dtAdapter.Fill(dtEmpFound);
                if (dtEmpFound.Rows.Count > 0)
                {
                    string oldpass = dtEmpFound.Rows[0]["PassWord"].ToString();
                    if (old_password.Value.ToString() == oldpass)
                    {
                        string newpass = re_new_password.Value.ToString();
                        strSQL = "Update Employee Set PassWord = '" + newpass + "', Email = '"+ email + "' where EmpID = '"+ Empid +"'";

                        objCmd.Connection = objConn;
                        objCmd.CommandText = strSQL;
                        objCmd.CommandType = CommandType.Text;
                        try
                        {
                            objCmd.ExecuteNonQuery();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(2);", true);
                        }
                        catch (Exception ex)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(3);", true);
                        }
                    }
                    else
                    {
                        old_password.Value = "";
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(4);", true);
                    }
                }
                else
                {
                    username.Value = "";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
                }
            }
            catch (Exception ex)
            {
                username.Value = "";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(1);", true);
            }
        }
    }
}