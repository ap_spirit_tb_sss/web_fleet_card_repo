﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Data;
using System.Threading;

namespace Fleet_Card
{
    public partial class ApproveOTBorrow : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        public static string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        string EmpID_Login = string.Empty;
        DataRow dr1, dr2;
        int i = 0, j = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                EmpID_Login = Session["EmpID"].ToString();
                //EmpID_Login = "11006142";  //นางสุดา ขันทอง
                //EmpID_Login = "11006121"; //โบตั๋น ศิริชัยวัฒน์

                if (!IsPostBack)
                {
                    DataTable dt1 = new DataTable(), dt1FormSQL = new DataTable();

                    dt1FormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser(EmpID_Login, "X", "X", 2, 2);
                    dt1 = Poolfunc.FilterDTApprove(dt1FormSQL, 2, this);
                    DatatableToHtmlTable1.Text = Poolfunc.ConvertDataTableToHTMLApprove(dt1, 1);
                }
            }
            catch (Exception ex)
            {

                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(3);", true);
                Response.Redirect("~/Login.aspx");
            }

        }

        protected void SubmitBtn_Click(object sender, EventArgs e)
        {
            string Submitstring = SubmitDocid.Value.ToString();
            if (Submitstring != "")
            {
                string[] submitdoc = Submitstring.Split('/');
                //////////////////////////////
                /// store_procedure_submit ///
                var Data = Poolfunc.Approved_All_Wolkflow(EmpID_Login, submitdoc[0], 1);
                //////////////////////////////
                SubmitDocid.Value = "";
                Thread.Sleep(3000);


                Page.ClientScript.RegisterStartupScript(this.GetType(), "Modal", "HidewaitupdateModal();", true);
                SubmitDocid.Value = "";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "ApprovedModal", "ShowApprovedModal();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
            }
        }

        protected void RejectBtn_Click(object sender, EventArgs e)
        {
            string Rejectstring = RejectDocid.Value.ToString();
            if (Rejectstring != "")
            {
                //////////////////////////////
                /// store_procedure_reject ///
                var Data = Poolfunc.Approved_All_Wolkflow(EmpID_Login, Rejectstring, 0);
                //////////////////////////////
                RejectDocid.Value = "";
                Thread.Sleep(3000);


                Page.ClientScript.RegisterStartupScript(this.GetType(), "Modal", "HidewaitupdateModal();", true);
                RejectDocid.Value = "";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "RejectedModal", "ShowRejectedModal();", true);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(1);", true);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {

            DataTable dt1 = new DataTable(), dt1FormSQL = new DataTable();

            dt1FormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser(EmpID_Login, "X", "X", 2, 2);
            dt1 = Poolfunc.FilterDTApprove(dt1FormSQL, 2, this);
            DatatableToHtmlTable1.Text = Poolfunc.ConvertDataTableToHTMLApprove(dt1, 1);

        }



    }
}