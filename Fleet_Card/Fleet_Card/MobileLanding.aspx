﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MobileLanding.aspx.cs" Inherits="Fleet_Card.MobileLanding" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CMS :: Application Download</title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link rel="apple-touch-icon" sizes="57x57" href="Images/Icon/apple-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="60x60" href="Images/Icon/apple-icon-60x60.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="Images/Icon/apple-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="Images/Icon/apple-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="Images/Icon/apple-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="Images/Icon/apple-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="Images/Icon/apple-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="Images/Icon/apple-icon-152x152.png" />
    <link rel="apple-touch-icon" sizes="180x180" href="Images/Icon/apple-icon-180x180.png" />
    <link rel="icon" type="image/png" sizes="192x192" href="Images/Icon/android-icon-192x192.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="Images/Icon/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="96x96" href="Images/Icon/favicon-96x96.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="Images/Icon/favicon-16x16.png" />
    <link rel="manifest" href="/manifest.json" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="msapplication-TileImage" content="Images/Icon/ms-icon-144x144.png" />
    <meta name="theme-color" content="#ffffff" />

    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <style>
        .landing-page::after {
            content: " ";
            background-image: linear-gradient(to bottom left, forestgreen, white);
            opacity: 0.5;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            position: absolute;
            z-index: -1;
        }
        .starttext{
            font-size: x-large; 
            font-weight: 800;
            color: dodgerblue;
        }
        .followtext{
                        font-size: x-large; 
            font-weight: 800;
            color:black;
        }
    </style>
</head>

<body class="landing-page">
    <form id="form1" runat="server">
        <div class="row">
            <div class="col-xs-12">
                <div id="label" align="center" style="margin-top: 20px;">
                    <label class="starttext">C</label><label class="followtext">ar&nbsp</label>
                    <label class="starttext">M</label><label class="followtext">anagement&nbsp</label>
                   <label class="starttext">S</label><label class="followtext">ystem</label>
                </div>
                <div id="Downloadlink" align="center" style="margin-top: 20px;">
                    <a href="https://drive.google.com/file/d/14paaB5gjvSkKu0TYNelw_uzHIsCIlsI2/view?usp=sharing"><img src="Images/android-button.png" width="250px"/></a>
                </div>
                <div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
