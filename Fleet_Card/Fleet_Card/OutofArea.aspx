﻿<%@ Page Title="CMS :: Out Of Area" Language="C#" EnableEventValidation="False" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OutofArea.aspx.cs" Inherits="Fleet_Card.OutofArea" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainForm")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("OutofAreaID")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link3")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .borderset {
            border-width: 1px;
            border-radius: 7px;
            border-color: grey;
            border-style: solid;
        }

        .select {
            color: #9e9e9e !important;
        }

        .option:not(:first-of-type) {
            color: black !important;
        }
    </style>
    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <link href="dist/calendarextenderdisable/CSS.css" rel="stylesheet" type="text/css" />
    <script src="dist/calendarextenderdisable/Extension.min.js" type="text/javascript"></script>

    <!-- SlimScroll -->
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <%--//Titleหัวข้อ--%>
    <section class="content-header">
        <h1>แบบฟอร์มนำรถไปจอดนอกพื้นที่
            <small>(beta)</small>
        </h1>
        <div style="margin-top: 10px;">
            <div style="margin-bottom: 10px;">
                <asp:Label ID="LabelDescript" runat="server" Text="กรุณากรอกข้อมูล ในช่องต่างๆให้ครบถ้วนและถูกต้อง"></asp:Label>
            </div>
            <div class="box">
                <div class="box-body">
                    <div class="form-group has-feedback" style="text-align: center;">
                        <h3 style="padding-top: 25px;"><%=Session["Company"].ToString() %> </h3>
                    </div>
                    <div class="form-group has-feedback" style="text-align: center;">
                        <h3>ใบขออนุญาตนำรถยนต์ไปจอดนอกพื้นที่</h3>
                    </div>

                    <div class="form-group has-feedback col-sm-12" style="float: right; text-align: center;">
                        <label class="form-group has-feedback col-sm-12" style="font-weight: normal;">ประจำวันที่ &nbsp&nbsp <%= DateTime.Now.ToString("dd/MM/yyyy") %></label>
                    </div>

                    <div class="form-group has-feedback col-sm-11 col-sm-offset-1" style="padding-bottom: 20px;">
                        <label class="form-group has-feedback col-sm-12" style="font-weight: normal;">
                            เนื่องด้วยข้าพเจ้า &nbsp&nbsp <b><u><%=Session["Name"].ToString() %>&nbsp&nbsp<%=Session["LastName"].ToString() %></u></b>&nbsp&nbsp&nbsp
                            ตำแหน่ง &nbsp&nbsp&nbsp<b><u><%=Session["EmpPosition"] %></u></b>&nbsp&nbsp&nbsp 
                            หน่วยงาน &nbsp&nbsp&nbsp <b><u><%=Session["Org"] %></u></b>&nbsp&nbsp&nbsp
                            พื้นที่/หน่วยขาย &nbsp&nbsp&nbsp <b><u><%=Session["Workplace"] %></u></b>&nbsp&nbsp&nbsp 
                        </label>
                        <div class="form-group has-feedback col-sm-12" style="float: left;">
                            <asp:Label ID="Label8" runat="server" Text="ปฏิบัติงานตั้งแต่เวลา<font color='red'>*</font>"></asp:Label>
                            <asp:TextBox ID="WorktimeStart" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" ClientIDMode="Static" />
                            <asp:Label ID="Label9" runat="server" Text="ถึงเวลา<font color='red'>*</font>"></asp:Label>
                            <asp:TextBox ID="WorktimeEnd" runat="server" CssClass="borderset" Style="text-align: center; width: 110px;" ClientIDMode="Static" />
                        </div>

                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label1" runat="server" Text="มีความประสงค์ขอนำรถยนต์คันหมายเลขทะเบียน<font color='red'>*</font>"></asp:Label>
                                <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="borderset" runat="server" AppendDataBoundItems="True" TabIndex="1">
                                    <Items>
                                        <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                    </Items>
                                </asp:DropDownList>
                            </div>
                            <div style="float: left; padding-left: 10px; padding-top: 6px;">
                                <asp:Label ID="Label7" runat="server" Text="ยี่ห้อ"></asp:Label>
                                <asp:TextBox ID="CarBrand" ClientIDMode="Static" CssClass="borderset" runat="server" Style="width: 110px; text-align: center;" />
                                <asp:Label ID="Label10" runat="server" Text="ไปจอดนอกพื้นที่บริษัทฯ/หน่วยขาย"></asp:Label>
                            </div>
                        </div>

                        <div class="form-group has-feedback col-sm-12">
                            <div style="float: left;">
                                <asp:Label ID="Label3" runat="server" Text="ระหว่างวันที่<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="StartDate" runat="server" CssClass="borderset" Style="text-align: center; width: 100px;" ClientIDMode="Static" onchange="buildEndDate();" />
                                <asp:Label ID="Label5" runat="server" Text="เวลา<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="StartTime" runat="server" CssClass="borderset" Style="text-align: center; width: 80px;" ClientIDMode="Static" />

                            </div>

                            <div style="float: left; padding-left: 5px;">
                                <asp:Label ID="Label4" runat="server" Text="ถึงวันที่<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="EndDate" runat="server" CssClass="borderset" Style="text-align: center; width: 100px;" Enabled="false" ClientIDMode="Static" />
                                <asp:Label ID="Label6" runat="server" Text="เวลา<font color='red'>*</font>"></asp:Label>
                                <asp:TextBox ID="EndTime" runat="server" CssClass="borderset" Style="text-align: center; width: 80px;" Enabled="false" ClientIDMode="Static" />
                            </div>
                            <%-- <div style="float: left; padding-left: 5px;">
                                <asp:Label ID="Label17" runat="server" Text="(จอดเฉพาะเสาร์-อาทิตย์"></asp:Label>
                            </div>
                            <div style="float: left; padding-top: 1px; padding-left: 5px;">
                                <asp:CheckBox ID="onlySatSun" runat="server" CssClass="" />
                            </div>
                            <div style="float: left; padding-left: 3px;">
                                <asp:Label ID="Label18" runat="server" Text=")"></asp:Label>
                            </div>--%>
                        </div>

                        <div class="form-group has-feedback col-sm-12" style="float: left; margin-top: 5px;">
                            <label style="font-weight: normal;">
                                <%--                                รวมเป็นจำนวน&nbsp
                                <label id="datedif" style="font-weight: bold;"></label>
                                &nbsp วัน &nbsp --%>
                                เพื่อความสะดวกในการปฏิบัติงานและเดินทางไป-กลับบ้านหลังปฏิบัติงานในพื้นที่รับผิดชอบเสร็จเรียบร้อยแล้ว
                                โดยนำจะรถไปจอด &nbsp ณ &nbsp สถานที่</label>
                        </div>
                        <div class="form-group has-feedback col-sm-12">
                            <asp:Label ID="Label2" runat="server" Text="บ้านเลขที่<font color='red'>*</font>"></asp:Label>
                            <asp:TextBox ID="HomeNumber" runat="server" CssClass="borderset" ClientIDMode="Static" />

                            <asp:Label ID="Label11" runat="server" Text="หมู่ที่"></asp:Label>
                            <asp:TextBox ID="VilNumber" runat="server" CssClass="borderset" ClientIDMode="Static" />

                            <asp:Label ID="Label12" runat="server" Text="ซอย"></asp:Label>
                            <asp:TextBox ID="AlleyName" runat="server" CssClass="borderset" ClientIDMode="Static" />

                            <asp:Label ID="Label13" runat="server" Text="ถนน"></asp:Label>
                            <asp:TextBox ID="RoadName" runat="server" CssClass="borderset" ClientIDMode="Static" />
                        </div>

                        <div class="form-group has-feedback col-sm-12">
                            <asp:Label ID="Label16" runat="server" Text="จังหวัด<font color='red'>*</font>"></asp:Label>
                            <asp:DropDownList ID="SelectProvince" ClientIDMode="Static" CssClass="borderset" runat="server" AppendDataBoundItems="True" Style="width: 175px;">
                                <Items>
                                    <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                </Items>
                            </asp:DropDownList>

                            <asp:Label ID="Label14" runat="server" Text="เขต/อำเภอ<font color='red'>*</font>"></asp:Label>
                            <asp:DropDownList ID="SelectSubDistrict" ClientIDMode="Static" Enabled="false" CssClass="borderset" runat="server" AppendDataBoundItems="True" Style="width: 175px;">
                                <Items>
                                    <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                </Items>
                            </asp:DropDownList>

                            <asp:Label ID="Label15" runat="server" Text="แขวง/ตำบล<font color='red'>*</font>"></asp:Label>
                            <asp:DropDownList ID="SelectDistrict" ClientIDMode="Static" Enabled="false" CssClass="borderset" runat="server" AppendDataBoundItems="True" Style="width: 175px;">
                                <Items>
                                    <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                </Items>
                            </asp:DropDownList>

                        </div>

                        <div class="form-group has-feedback col-sm-12" style="margin-top: 20px;">
                            <label style="font-weight: normal;">
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp โดยในวันจันทร์ - ศุกร์ จะนำรถไปจอดที่บ้านพัก &nbsp และภายในวันเสาร์หลังเลิกงาน &nbsp หรือในวันหยุดนักขัตฤกษ์จะนำรถมาจอดที่ออฟฟิต 
                            และจะมานำรถไปจอดที่บ้านพักอีกครั้งในวันจันทร์หรือวันทำงานวันแรกของสัปดาห์</label>
                        </div>
                        <div class="form-group has-feedback col-sm-12">
                            <label style="font-weight: normal;">
                                &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp โดยข้าพเจ้าขอรับรองว่าสถานที่จอดรถ &nbsp มีความปลอดภัยเป็นอย่างดี &nbsp 
                            และข้าพเจ้าจะไม่นำรถยนต์ของทางบริษัทฯไปใช้ในการต่างๆอื่นใดที่ไม่ใช่ของบริษัทฯ
                            &nbsp และใช้งานนอกเส้นทางโดยเด็ดขาด &nbsp ซึ่งหากเกิดปัญหาใดๆกับรถยนต์คันดังกล่าวข้างต้น
                            ข้าพเจ้ายินดีรับผิดชอบความเสียหายนั้นทุกประการ
                            </label>
                        </div>
                        <div class="form-group has-feedback col-sm-1" style="text-align: center;">
                        </div>

                    </div>
                </div>

                <div style="margin-top: 15px;">
                    <div class="form-group has-feedback col-sm-6" style="padding-left: 30%; padding-right: 5%;">
                        <asp:Button ID="ConfrimBtn" runat="server" Text="Confirm" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="ConfrimBtn_Click" />
                    </div>

                    <div class="form-group has-feedback col-sm-6" style="padding-left: 5%; padding-right: 30%;">
                        <asp:Button ID="CancelBtn" runat="server" Text="Cancel" CssClass="btn btn-danger btn-block btn-flat" OnClick="CancelBtn_Click" />
                    </div>
                </div>
                <asp:HiddenField ID="HiddenAmphur" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HiddenDistrict" ClientIDMode="Static" runat="server" />
            </div>
        <!-- Modal -->
                <div class="modal fade " id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="col-sm-3">
                                    <div class="col-sm-9">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                                    </div>
                                </div>
                                <h4 class="modal-title">กรุณารอซักครู่กำลังอัพโหลดแบบฟอร์มของท่าน</h4>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </section>

    <script>

        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'กรุณากรอกข้อมูลให้ครบถ้วน ก่อนกดปุ่มยืนยัน';
                    break;
                case 1:
                    MessageAlert = 'อัพโหลดฟอร์มการขอยืมรถสำเร็จ';
                    break;
                case 2:
                    MessageAlert = 'อัพโหลดฟอร์มการขอยืมรถ ผิดพลาด \nกรุณาติดต่อผู้ดูแลระบบ';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }

         <%--ฟังก์ชั่นไม่ให้เลือกวันที่น้อยกว่าวันที่จาก Startdate --%>
        $(function () {


            $("#SelectSubDistrict").select2({
                placeholder: {
                    id: "0",
                    text: "เลือกเขต/อำเภอ"
                },
                allowClear: true,
                width: '175px'
            })

            $("#SelectDistrict").select2({
                placeholder: {
                    id: "0",
                    text: "เลือกแขวง/ตำบล"
                },
                allowClear: true,
                width: '175px'
            })

            $("#StartDate").datepicker({
                dateFormat: "dd/mm/yy",
                minDate: "+0d"
            });

            $("#EndDate").datepicker({ dateFormat: "dd/mm/yy" });

            $("#WorktimeStart").timepicker(
            );

            $("#WorktimeEnd").timepicker(
            );

            $("#StartTime").timepicker(
            );

            $("#EndTime").timepicker(
            );

            document.getElementById("datedif").innerHTML = 0;
            document.getElementById("StartTime").value = "";
            document.getElementById("EndTime").value = "";

        });

        function buildEndDate() {
            if ($("#StartDate").val() != "") {
                document.getElementById("EndDate").disabled = false;
                document.getElementById("EndDate").value = "";
                document.getElementById("EndTime").disabled = false;
                document.getElementById("EndTime").value = "";

                $("#EndDate").datepicker('option', 'minDate', new Date($("#StartDate").datepicker("getDate")));

                var MDate = new Date($("#StartDate").datepicker("getDate"));
                MDate.setDate(MDate.getDate() + 60);
                console.log(MDate)
                $("#EndDate").datepicker('option', 'maxDate', MDate);

                $("#EndDate").datepicker('refresh');

            }
            else {
                document.getElementById("EndDate").disabled = true;
                document.getElementById("EndDate").value = "";
                document.getElementById("EndTime").disabled = true;
                document.getElementById("EndTime").value = "";
            }
        }

         <%--ฟังก์ชั่นปิดการทำงานช่อง Enddate กรณี StartDate ไม่กรอก --%>
        //$("#StartDate").change(function () {
        //    if ($("#StartDate").val() != "") {
        //        document.getElementById("EndDate").disabled = false;
        //        document.getElementById("EndDate").value = "";

        //        var dateField = $("#EndDate");
        //        dateField.datepicker('option', 'minDate', new Date($("#StartDate").datepicker("getDate")));

        //        var MDate = new Date($("#StartDate").datepicker("getDate"));
        //        MDate.setDate(MDate.getDate() + 30);
        //        dateField.datepicker('option', 'maxDate', MDate);

        //        dateField.datepicker('refresh');

        //    }
        //    else {
        //        document.getElementById("EndDate").disabled = true;
        //        document.getElementById("EndDate").value = "";
        //    }
        //});

        //$("#EndDate").change(function () {
        //    if ($("#EndDate").val() != "" && $("#StartDate").val() != "") {
        //        var start = $("#StartDate").datepicker('getDate');
        //        var end = $("#EndDate").datepicker('getDate');
        //        var days = (end - start) / 1000 / 60 / 60 / 24;
        //        document.getElementById("datedif").innerHTML = days;
        //    }
        //    else {
        //        document.getElementById("datedif").innerHTML = 0;
        //    }
        //});

        <%--ฟังก์ชั่นเช็คช่องข้อมูลที่ยังไม่กรอกตอนกด confrim --%>
        function checknotchoose() {
            var Plate = $('#CarPlate').prop('selectedIndex');
            var Brand = $('#CarBrand').val();
            var WorktimeStart = $('#WorktimeStart').val();
            var WorktimeEnd = $('#WorktimeEnd').val();
            var StartDate = $('#StartDate').val();
            var EndDate = $('#EndDate').val();
            var StartTime = $('#StartTime').val();
            var EndTime = $('#EndTime').val();
            var HomeNumber = $('#HomeNumber').val();
            var Province = $('#SelectProvince').prop('selectedIndex')
            var SubDistrict = $('#SelectSubDistrict').prop('selectedIndex')
            var District = $('#SelectDistrict').prop('selectedIndex')

            if (Plate > 0 & Province > 0 & SubDistrict > 0 & District > 0 & HomeNumber != "" & $.trim(StartDate) != "" & $.trim(EndDate) != "" & $.trim(StartTime) != "" & $.trim(EndTime) != "") {
                $('#myModal').modal({ backdrop: 'static', keyboard: false })
                return true;
            }
            else if ($.trim(WorktimeStart) == "") {
                alert('กรุณาระบุเวลาเริ่มต้นการทำงาน');
                $('#WorktimeStart').focus();
                $('#WorktimeStart').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#WorktimeStart').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(WorktimeEnd) == "") {
                alert('กรุณาระบุเวลาสิ้นสุดการทำงาน');
                $('#WorktimeEnd').focus();
                $('#WorktimeEnd').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#WorktimeEnd').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if (Plate <= 0) {
                alert('กรุณากรอกทะเบียนรถที่ท่านต้องการจะยืม');
                $('#CarPlate').select2('open');
                return false;
            }
            else if ($.trim(Brand) == "") {
                alert('กรุณาเลือกยี่ห้อรถยนต์');
                $('#BrandDdl').focus();
                $('#BrandDdl').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#BrandDdl').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }


            else if ($.trim(StartDate) == "") {
                alert('กรุณาเลือกวันที่เริ่มต้นในการยืมรถ');
                $('#StartDate').focus();
                $('#StartDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(EndDate) == "") {
                alert('กรุณาเลือกวันที่จะทำการคืนรถที่ยืมไป');
                $('#EndDate').focus();
                $('#EndDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#EndDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(StartTime) == "") {
                alert('กรุณาเลือกเวลาที่เริ่มต้นในการยืมรถ');
                $('#StartTime').focus();
                $('#StartTime').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartTime').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
            else if ($.trim(EndTime) == "") {
                alert('กรุณาเลือกเวลาที่จะทำการคืนรถที่ยืมไป');
                $('#EndTime').focus();
                $('#EndTime').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#EndTime').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });

                return false;
            }

            else if ($.trim(HomeNumber) == "") {
                alert('กรุณากรอกบ้านเลขที่');
                $('#HomeNumber').focus();
                $('#HomeNumber').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#HomeNumber').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }

            else if (Province <= 0) {
                alert('กรุณาเลือกจังหวัดที่อยู่');
                $('#SelectProvince').select2('open');
                return false;
            }
            else if (SubDistrict <= 0) {
                alert('กรุณาเลือกอำเภอที่อยู่');
                $('#SelectSubDistrict').select2('open');
                return false;
            }
            else /*if (District <= 0)*/ {
                alert('กรุณาเลือกตำบลที่อยู่');
                $('#SelectDistrict').select2('open');
                return false;
            }
        };

        function HideModal() {
            $('#myModal').modal('hide');
        }

        <%--ฟังก์ชั่น Select2 --%>

        $(function () {

            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '150px'
            });

            $("#SelectProvince").select2({
                placeholder: {
                    id: "0",
                    text: "เลือกจังหวัด"
                },
                allowClear: true,
                width: '175px'
            });
        });


        <%-- Ajax --%>
        <%-- Ajax CarBrand--%>

        $("#CarPlate").change(function () {

            var carid = $("#CarPlate").val();
            var JData = { CarID: carid };

            $.ajax({
                type: 'POST',
                url: 'OTBorrowForm.aspx/CarBrandFind',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(JData),
                dataType: 'json',
                success: function (data) {
                    document.getElementById("CarBrand").value = JSON.parse(data.d);
                },
                failure: function (result) {
                    alert(result.d);
                },
                error: function (xhr, status, error) {
                    alert('Exception:Ajax Json Error = ' + error, error);
                }


            });
        });

            <%-- Ajax Amphur--%>
        $("#SelectProvince").change(function () {

            var proval = $("#SelectProvince").val();
            var JData = { ProVal: proval, AmpVal: "0", Type: "1" };

            callajaxAmp(JData);
        });



        function callajaxAmp(JData) {
            $.ajax({

                type: 'POST',
                url: 'OutofArea.aspx/FilterAmphurandDistrict',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(JData),
                dataType: 'json',
                success: OnSucccessAmp,
                failure: function (result) {
                    alert(result.d);
                },
                error: function (xhr, status, error) {
                    alert('Exception:Ajax Json Error = ' + error, error);
                }
            });
        }

        function OnSucccessAmp(data) {

            if (data.d != "") {
                $('#SelectDistrict').val("0").trigger('change');
                $('#SelectSubDistrict').prop("disabled", false);
                $('#SelectDistrict').prop("disabled", true);
                var selectobject = $('#SelectSubDistrict option');
                if (selectobject.length > 1) {
                    selectobject.remove();
                    selectobject.add();
                }

                var selectobject = $('#SelectSubDistrict option');

                var selectobject2 = $('#SelectSubDistrict option')
                if (selectobject2.length > 1) {
                    selectobject2.remove();
                    selectobject2.add();
                }


                $("#SelectSubDistrict").select2({
                    data: JSON.parse(data.d),
                    placeholder: {
                        id: "0",
                        text: "เลือกเขต/อำเภอ"
                    },
                    allowClear: true,
                    width: '175px'
                })
            }
            else {
                $('#SelectSubDistrict').val("0").trigger('change');
                $('#SelectDistrict').val("0").trigger('change');
                $('#SelectSubDistrict').prop("disabled", true);
                $('#SelectDistrict').prop("disabled", true);


                $("#SelectSubDistrict").select2({
                    data: null,
                    placeholder: {
                        id: "0",
                        text: "เลือกเขต/อำเภอ"
                    },
                    allowClear: true,
                    width: '175px'
                })

                $("#SelectDistrict").select2({
                    data: null,
                    placeholder: {
                        id: "0",
                        text: "เลือกแขวง/ตำบล"
                    },
                    allowClear: true,
                    width: '175px'
                })

            }

        }


            <%-- Ajax District--%>
        $("#SelectSubDistrict").change(function () {

            var proval = $("#SelectProvince").val();
            var ampval = $("#SelectSubDistrict").val();
            var JDataDist = { ProVal: proval, AmpVal: ampval, Type: 2 };

            callajaxDist(JDataDist);
        });


        function callajaxDist(JDataDist) {
            $.ajax({

                type: 'POST',
                url: 'OutofArea.aspx/FilterAmphurandDistrict',
                contentType: 'application/json; charset=utf-8',
                async: false,
                data: JSON.stringify(JDataDist),
                dataType: 'json',
                success: OnSucccessDist,
                failure: function (result) {
                    alert(result.d);
                },
                error: function (xhr, status, error) {
                    alert('Exception:Ajax Json Error = ' + error, error);
                }
            });
        }


        function OnSucccessDist(data) {
            if (data.d != "") {

                $('#SelectDistrict').prop("disabled", false);

                var selectobject = $('#SelectDistrict option');
                if (selectobject.length > 1) {
                    selectobject.remove();
                    selectobject.add();
                }


                $("#SelectDistrict").select2({
                    data: JSON.parse(data.d),
                    placeholder: {
                        id: "0",
                        text: "เลือกแขวง/ตำบล"
                    },
                    allowClear: true,
                    width: '175px'
                })
                var textAmphur = $("#SelectSubDistrict").select2("data")[0].text;
                $("#HiddenAmphur").val(textAmphur);
            }
            else {
                $('#SelectDistrict').val("0").trigger('change');
                $('#SelectDistrict').prop("disabled", true);

                $("#SelectDistrict").select2({
                    data: null,
                    placeholder: {
                        id: "0",
                        text: "เลือกแขวง/ตำบล"
                    },
                    allowClear: true,
                    width: '175px'
                })
            }

            $("#SelectDistrict").change(function () {
                var textdistrict = $("#SelectDistrict").select2("data")[0].text;
                $("#HiddenDistrict").val(textdistrict);
            });

        }



    </script>
</asp:Content>
