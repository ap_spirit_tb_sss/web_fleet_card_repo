﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarMileReport.aspx.cs" Inherits="Fleet_Card.Report.CarMileReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainReport")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("CarMileReport")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link11")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .borderset {
            border-width: 1px;
            border-radius: 7px;
            border-color: grey;
            border-style: solid;
        }
    </style>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.css">
    <!-- DataTables.boostrap -->
    <link rel="stylesheet" href="../bower_components/Datatable_New/DataTables-1.10.18/css/dataTables.bootstrap.css">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="../bower_components/Datatable_New/datatables.css">
    <!-- DataTables Responsive boostrap -->
    <link rel="stylesheet" href="../bower_components/Datatable_New/Responsive-2.2.2/css/responsive.bootstrap.css" />
    <!-- DataTables Checkbox-Col -->
    <link href="../bower_components/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- jQuery-UI -->
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet" />
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.structure.css" rel="stylesheet" />
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.theme.css" rel="stylesheet" />
    <script src="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <!-- jQuery-UI : Time Picker -->
    <link href="../bower_components/Time-Selection/dist/css/timepicker.css" rel="stylesheet" />
    <script src="../bower_components/Time-Selection/dist/js/timepicker.js"></script>
    <!-- Select2 -->
    <link href="../bower_components/select2/dist/css/select2.css" rel="stylesheet" />
    <script src="../bower_components/select2/dist/js/select2.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.js"></script>
    <%--    <script src="dist/js/adminlte.min.js"></script>--%>
    <!-- ChartJS NEW -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <!-- ChartJS Plugin -->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
    <!-- FastClick -->
    <%--<script src="../bower_components/fastclick/lib/fastclick.js"></script>--%>
    <!-- DataTables.boostrap -->
    <script src="../bower_components/Datatable_New/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="../bower_components/Datatable_New/datatables.js"></script>
    <!-- DataTables Responsive boostrap -->
    <script src="../bower_components/Datatable_New/Responsive-2.2.2/js/responsive.bootstrap.js"></script>
    <!-- DataTables Responsive -->
    <script src="../bower_components/Datatable_New/Responsive-2.2.2/js/dataTables.responsive.js"></script>
    <!-- DataTables checkbox-col-->
    <script src="../bower_components/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.js"></script>

    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <!-- SlimScroll -->
    <script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <%--//Titleหัวข้อ--%>

    <section class="content-header">
        <h1>รายงานประวัติการใช้งานรถ
            <small>(beta)</small>
        </h1>

        <div class="box" style="margin-top: 20px;">
            <div class="box-body">
                <div class="col-sm-3">
                    <div>
                        <asp:Label ID="Label1" runat="server" Text="กรุณาเลือกเดือนสำหรับการดึงรายงาน<font color='red'>*</font>"></asp:Label>
                    </div>
                    <div style="margin-top: 8px;">
                        <asp:TextBox ID="StartDate" runat="server" CssClass="form-control" Style="text-align: center; width: 200px; background-color: white;" ClientIDMode="Static" onchange="buildEndDate();" TabIndex="2" />
                    </div>

                </div>

                <div class="col-sm-3">
                    <div>
                        <asp:Label ID="Label2" runat="server" Text="กรุณาเลือกป้ายทะเบียนที่ต้องการ"></asp:Label>
                    </div>
                    <div style="margin-top: 8px;">
                        <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="form-control" Style="width: 200px;" runat="server" AppendDataBoundItems="True" TabIndex="1">
                            <Items>
                                <asp:ListItem Text="PLACEHOLDER" Value="0" />
                            </Items>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div>
                        <asp:Label ID="Label3" runat="server" Text="กรุณากรอกรหัสพนักงานที่ต้องการ"></asp:Label>
                    </div>
                    <div style="margin-top: 8px;">
                        <asp:TextBox ID="TxtEmpID" runat="server" CssClass="form-control" Width="200px" onkeypress="return allowOnlyNumber(event);" MaxLength="8"></asp:TextBox>
                    </div>
                </div>

                <div class="col-sm-3" style="padding-top: 28px;">
                    <asp:Button ID="ViewBtn" runat="server" Text="View" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="ViewBtn_Click" />
                </div>

            </div>
        </div>

        <div id="divReport" name="divReport" runat="server" class="box" visible="false" style="margin-top: 20px;">
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="" ClientIDMode="AutoID" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" Width="100%" Height="425px" ZoomPercent="90" ShowBackButton="False" ShowRefreshButton="False" SizeToReportContent="False" ShowZoomControl="False" ShowPageNavigationControls="False">
                        <ServerReport ReportPath="Report/CarMile_Report.rdlc" ReportServerUrl="" />
                        <LocalReport ReportPath="Report/CarMile_Report.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>--%>
                </div>
            </div>
        </div>

        <div id="divNoData" name="divReport" runat="server" class="box" visible="false" style="margin-top: 20px;" align="center">
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/NODATA.png" Height="70" Width="70" />
                    <asp:Label ID="Nodatalabel" runat="server" Text="ไม่พบข้อมูลจากตัวเลือกที่ท่านต้องการ กรุณาเลือกตัวเลือกใหม่อีกครั้ง " Font-Bold="True" Font-Size="Larger"></asp:Label>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="HiddenDownloadMonth" ClientIDMode="Static" runat="server" />

    </section>

    <script>

         <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'Internal Error: Monthstr didnt contain any value.\nPlease contact administrator for solution.';
                    break;
                case 1:
                    MessageAlert = '';
                    break;
                case 2:
                    MessageAlert = '';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }

        $(function () {
            $("#StartDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm/yy",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month - 1, 1)));
                },
                onClose: function (dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
                }
            })
                .focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });
        });

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57) {
                return false;
            }

        }

        $(function () {

            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '200px'

            });
        });

        function checknotchoose() {

            var StartDate = $('#StartDate').val();

            if ($.trim(StartDate) != "") {
                return true;
            }

            else if ($.trim(StartDate) == "") {
                alert('กรุณาเลือกเดือนที่ต้องการดูรายงาน');
                $('#StartDate').focus();
                $('#StartDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
        }

    </script>
</asp:Content>

