﻿<%@ Page Title="CMS :: Login" Language="C#" MasterPageFile="~/SiteLogin.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Fleet_Card.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="dist/css/skins/skin-blue.css">
    <!-- DataTables.boostrap -->
    <link rel="stylesheet" href="bower_components/Datatable_New/DataTables-1.10.18/css/dataTables.bootstrap.css">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="bower_components/Datatable_New/datatables.css">
    <!-- DataTables Responsive boostrap -->
    <link rel="stylesheet" href="bower_components/Datatable_New/Responsive-2.2.2/css/responsive.bootstrap.css" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">



<%--    <link rel="shortcut icon"  href="Images/Icon/fleeticon.ico"/>
    <link rel="icon" sizes="16x16 32x32 64x64" href="Images/Icon/fleeticon.ico">--%>
    <%--<link rel="mobile-web-app-capable"  href="~/Images/Icon/fleeticon.ico">--%>

    <div id="loginbox" name="loginbox" class="login-box">
        <div class="login-box-body">
            <div class="login-logo">
                <h3 style="color: white;"><b>Car &nbsp Management &nbsp System</b></h3>
            </div>
            <p class="login-box-msg">Sign in with your ID</p>
            <div class="form-group has-feedback">
                <input runat="server" id="username" class="form-control" placeholder="Employee ID" maxlength="8" tabindex="1" data-toggle="tooltip" data-placement="left" title="กรอกหมายเลขพนักงานของท่าน"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
                <input runat="server" id="password" type="password" class="form-control" placeholder="Password" maxlength="16" tabindex="2" data-toggle="tooltip" data-placement="left" title="กรอกรหัสผ่านเก่าของท่าน">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <%--                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox">
                            Remember Me
                        </label>
                    </div>--%>
                </div>
                <div class="col-xs-4">
                    <asp:Button ID="SigninBtn" runat="server" Text="Sign In" CssClass="btn btn-primary btn-block btn-flat" OnClick="SigninBtn_Click1" TabIndex="3" />
                </div>
            </div>
            <div align="right" style="margin-top: 30px;" tabindex="4">
                <a href="ChangePassword.aspx" style="color:white;">Change password <i class="fa fa-refresh"></i></a>
            </div>
        </div>
    </div>
    <script>

        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'รหัสพนักงานหรือรหัสผ่านผิด\nกรุณาตรวจสอบรหัสพนักงานและรหัสผ่านให้ถูกต้อง';
                    break;
                case 1:
                    MessageAlert = 'อัพโหลดฟอร์มการขอยืมรถสำเร็จ';
                    break;
                case 2:
                    MessageAlert = 'อัพโหลดฟอร์มการขอยืมรถ ผิดพลาด \nกรุณาติดต่อผู้ดูแลระบบ';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }

    </script>

</asp:Content>
