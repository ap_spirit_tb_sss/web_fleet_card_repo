﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;
using Microsoft.Reporting.WebForms;

namespace Fleet_Card
{
    public partial class borrowcardreport : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlDataReader readersql;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        string EmpID_Login = string.Empty;
        DataTable dtAllcarandcard = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            StartDate.Attributes.Add("ReadOnly", "true");

            if (!Page.IsPostBack)
            {
                var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
                objConn.ConnectionString = strConnString;
                objConn.Open();

                //GET_Asset_Data();

                int Region = int.Parse(Session["Region"].ToString());

                strSQL = "Select * from View_CarBrand where Region = " + Region;
                dtAdapter = new SqlDataAdapter(strSQL, objConn);
                dtAdapter.Fill(dtAllcarandcard);

                ViewState["dtAllcarandcard"] = dtAllcarandcard;
                CarPlate.DataSource = dtAllcarandcard;
                CarPlate.DataTextField = "LicensePlate_Car";
                CarPlate.DataValueField = "CarID";
                CarPlate.DataBind();

                Fleetcard.DataSource = dtAllcarandcard;
                Fleetcard.DataTextField = "FleetCardNo";
                Fleetcard.DataValueField = "FleetCardNo";
                Fleetcard.DataBind();

                objConn.Close();
            }
        }

        protected void ViewBtn_Click(object sender, EventArgs e)
        {
            string monthstr = StartDate.Text.ToString();
            string fleetcard = "X";
            string carplate = "X";
            string empid = "X";

            if (monthstr != "")
            {
                if (Fleetcard.SelectedIndex > 0)
                {
                    fleetcard = Fleetcard.SelectedItem.ToString();
                }

                if (CarPlate.SelectedIndex > 0)
                {
                    carplate = CarPlate.SelectedItem.ToString();
                }

                if (TxtEmpID.Text != "")
                {
                    empid = TxtEmpID.Text.ToString();
                }

                ///////////////////////////////////////
                ///// รอโค้ดการ Query จาก Database /////
                GET_Asset_Data(empid, carplate, monthstr, fleetcard);
                ///////////////////////////////////////
                //Thread.Sleep(5000);

            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true);
            }
        }

        public void GET_Asset_Data(string empsearch, string plateno, string monthstr,string fleetcard) //(string CompCode, string AssetClass, string AssetStatus, string EmpID_Login)
        {
            DataTable dtMonthReport = new DataTable();
            string Region = Session["Region"].ToString().Trim();
            string Empid = Session["EmpID"].ToString().Trim();
            DataSet DS = new DataSet();

            dtMonthReport = Poolfunc.GET_Trans_Card_Borrowing_Monthly_Report(Empid, empsearch, "X", plateno, fleetcard, monthstr, Region, "X", "X");

            //var adapter = new SqlDataAdapter(dCmd);

            //adapter.Fill(DS, "DataSet_Report");      
            if (dtMonthReport.Rows.Count > 0)
            {
                if (divNoData.Visible == true)
                {
                    divNoData.Visible = false;
                }

                divReport.Visible = true;

                string[] monthset = monthstr.Split('/');

                string monthname = Poolfunc.findmonthname(monthset[0]);

                string fullmonth = monthname + "/" + monthset[1];

                ReportParameter[] parms = new ReportParameter[1];
                parms[0] = new ReportParameter("paramMonthstr", fullmonth);
                ReportViewer1.LocalReport.SetParameters(parms);

                DS.Tables.Add(dtMonthReport);
                DS.Tables[0].TableName = "DataSet_Report";
                ReportDataSource rds = new ReportDataSource("DataSet_Borrowing_Card", DS.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(rds);

                ReportViewer1.LocalReport.Refresh();
            }
            else
            {
                if (divReport.Visible == true)
                {
                    divReport.Visible = false;
                }
                divNoData.Visible = true;
            }
        }
    }
}