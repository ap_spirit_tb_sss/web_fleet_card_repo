﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Data;

namespace Fleet_Card
{
    public partial class MainPage : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        string EmpID_Login = string.Empty;
        DataRow dr1, dr2;
        int i = 0, j = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                EmpID_Login = Session["EmpID"].ToString();


                if (!IsPostBack)
                {
                    DataTable dt1FormSQL = new DataTable(), dt2FormSQL = new DataTable(), dt3FormSQL = new DataTable(), dt1 = new DataTable(), dt2 = new DataTable(), dt3 = new DataTable();


                    dt1FormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser("X", EmpID_Login, "X", -1, 2);
                    dt1 = Poolfunc.FilterDTMain(dt1FormSQL, 2, this);
                    //DataView dv_Ap1 = new DataView(dt1FormSQL);
                    //dv_Ap1.RowFilter = "Status = 6"; 

                    dt2FormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser("X", EmpID_Login, "X", -1, 1);
                    dt2 = Poolfunc.FilterDTMain(dt2FormSQL, 1, this);
                    //DataView dv_Ap2 = new DataView(dt2FormSQL);
                    //dv_Ap2.RowFilter = "Status = 6"; 

                    dt3FormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser("X", EmpID_Login, "X", -1, 3);
                    dt3 = Poolfunc.FilterDTMain(dt3FormSQL, 3, this);
                    //DataView dv_Ap3 = new DataView(dt3FormSQL);
                    //dv_Ap3.RowFilter = "Status = 6";

                    DatatableToHtmlTable1.Text = Poolfunc.ConvertDataTableToHTMLMain(dt1, 1);
                    DatatableToHtmlTable2.Text = Poolfunc.ConvertDataTableToHTMLMain(dt2, 2);
                    DatatableToHtmlTable3.Text = Poolfunc.ConvertDataTableToHTMLMain(dt3, 3);
                }
            }

            catch (Exception ex)
            {
                Response.Write("<script> alert('Error :: '" + ex.Message + "'')</script>");
                Response.Redirect("~/Login.aspx");
            }
        }



    }
}