﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BorrowCardReport.aspx.cs" Inherits="Fleet_Card.borrowcardreport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainReport")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("BorrowingCardReport")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link14")
            currentlink.removeAttribute("href"); B

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content-header">
        <h1>รายงานการยืมบัตร Fleet Card
            <small>(beta)</small>
        </h1>

        <div class="box" style="margin-top: 20px;">
            <div class="box-body">
                <div class="col-sm-9">
                    <div class="col-sm-6">
                        <div>
                            <asp:Label ID="Label1" runat="server" Text="กรุณาเลือกเดือนสำหรับการดึงรายงาน<font color='red'>*</font>"></asp:Label>
                        </div>
                        <div style="margin-top: 4px;">
                            <asp:TextBox ID="StartDate" runat="server" CssClass="form-control" Style="text-align: center; width: 200px; background-color: white;" ClientIDMode="Static" onchange="buildEndDate();" TabIndex="2" />
                        </div>

                    </div>

                    <div class="col-sm-6">
                        <div>
                            <asp:Label ID="Label4" runat="server" Text="กรุณาเลือกหมายเลข Fleet Card ที่ต้องการ"></asp:Label>
                        </div>
                        <div style="margin-top: 4px;">
                            <asp:DropDownList ID="Fleetcard" ClientIDMode="Static" CssClass="form-control" Style="width: 200px;" runat="server" AppendDataBoundItems="True" TabIndex="1">
                                <Items>
                                    <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                </Items>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div style="margin-top: 8px;">
                            <asp:Label ID="Label2" runat="server" Text="กรุณาเลือกป้ายทะเบียนที่ต้องการ"></asp:Label>
                        </div>
                        <div style="margin-top: 4px;">
                            <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="form-control" Style="width: 200px;" runat="server" AppendDataBoundItems="True" TabIndex="1">
                                <Items>
                                    <asp:ListItem Text="PLACEHOLDER" Value="0" />
                                </Items>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div style="margin-top: 8px;">
                            <asp:Label ID="Label3" runat="server" Text="กรุณากรอกรหัสพนักงานที่ต้องการ"></asp:Label>
                        </div>
                        <div style="margin-top: 4px;">
                            <asp:TextBox ID="TxtEmpID" runat="server" CssClass="form-control" Width="200px" onkeypress="return allowOnlyNumber(event);" MaxLength="8"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-sm-3" style="padding-top: 50px;">
                    <asp:Button ID="ViewBtn" runat="server" Text="View" CssClass="btn btn-success btn-block btn-flat" OnClientClick="return checknotchoose();" OnClick="ViewBtn_Click" />
                </div>

            </div>
        </div>

        <div id="divReport" name="divReport" runat="server" class="box" visible="false" style="margin-top: 20px;">
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" BackColor="" ClientIDMode="AutoID" HighlightBackgroundColor="" InternalBorderColor="204, 204, 204" InternalBorderStyle="Solid" InternalBorderWidth="1px" LinkActiveColor="" LinkActiveHoverColor="" LinkDisabledColor="" PrimaryButtonBackgroundColor="" PrimaryButtonForegroundColor="" PrimaryButtonHoverBackgroundColor="" PrimaryButtonHoverForegroundColor="" SecondaryButtonBackgroundColor="" SecondaryButtonForegroundColor="" SecondaryButtonHoverBackgroundColor="" SecondaryButtonHoverForegroundColor="" SplitterBackColor="" ToolbarDividerColor="" ToolbarForegroundColor="" ToolbarForegroundDisabledColor="" ToolbarHoverBackgroundColor="" ToolbarHoverForegroundColor="" ToolBarItemBorderColor="" ToolBarItemBorderStyle="Solid" ToolBarItemBorderWidth="1px" ToolBarItemHoverBackColor="" ToolBarItemPressedBorderColor="51, 102, 153" ToolBarItemPressedBorderStyle="Solid" ToolBarItemPressedBorderWidth="1px" ToolBarItemPressedHoverBackColor="153, 187, 226" Width="100%" Height="425px" ZoomPercent="90" ShowBackButton="False" ShowRefreshButton="False" SizeToReportContent="False" ShowZoomControl="False" ShowPageNavigationControls="False">
                        <ServerReport ReportPath="Report/BorrowingCar_Report.rdlc" ReportServerUrl="" />
                        <LocalReport ReportPath="Report/BorrowingCar_Report.rdlc">
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>--%>
                </div>
            </div>
        </div>

        <div id="divNoData" name="divReport" runat="server" class="box" visible="false" style="margin-top: 20px;" align="center">
            <div class="box-body">
                <div class="box-body col-lg-12">
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/NODATA.png" Height="70" Width="70" />
                    <asp:Label ID="Nodatalabel" runat="server" Text="ไม่พบข้อมูลจากตัวเลือกที่ท่านต้องการ กรุณาเลือกตัวเลือกใหม่อีกครั้ง " Font-Bold="True" Font-Size="Larger"></asp:Label>
                </div>
            </div>
        </div>

        <asp:HiddenField ID="HiddenDownloadMonth" ClientIDMode="Static" runat="server" />

        <!-- Modal -->
        <div class="modal fade " id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-sm-3">
                            <div class="col-sm-9">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ajax-loader.gif" />
                            </div>
                        </div>
                        <h4 class="modal-title">กรุณารอซักครู่ กำลังจัดเตรียมไฟล์รายงานของท่านอยู่</h4>
                    </div>
                    <%--                <div class="modal-body">

                </div>--%>
                </div>

            </div>
        </div>
        <!-- End  Modal -->

    </section>

    <script>

         <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>
        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = 'Internal Error: Monthstr didnt contain any value.\nPlease contact administrator for solution.';
                    break;
                case 1:
                    MessageAlert = '';
                    break;
                case 2:
                    MessageAlert = '';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }

        $(function () {
            $("#StartDate").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "mm/yy",
                showButtonPanel: true,
                currentText: "This Month",
                onChangeMonthYear: function (year, month, inst) {
                    $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month - 1, 1)));
                },
                onClose: function (dateText, inst) {
                    var month = $(".ui-datepicker-month :selected").val();
                    var year = $(".ui-datepicker-year :selected").val();
                    $(this).val($.datepicker.formatDate('mm/yy', new Date(year, month, 1)));
                }
            })
                .focus(function () {
                    $(".ui-datepicker-calendar").hide();
                });
        });

        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57) {
                return false;
            }

        }

        $(function () {

            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '200px'

            });

            $("#Fleetcard").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกหมายเลขบัตร',
                },
                allowClear: true,
                width: '200px'

            });
        });

        function checknotchoose() {

            var StartDate = $('#StartDate').val();

            if ($.trim(StartDate) != "") {
                return true;
            }

            else if ($.trim(StartDate) == "") {
                alert('กรุณาเลือกเดือนที่ต้องการดูรายงาน');
                $('#StartDate').focus();
                $('#StartDate').focus(function () {
                    $(this).css("border", "#ff3300 solid 3px");
                });
                $('#StartDate').blur(function () {
                    $(this).css("border", "#999999 solid 1px");
                });
                return false;
            }
        }

    </script>
</asp:Content>
