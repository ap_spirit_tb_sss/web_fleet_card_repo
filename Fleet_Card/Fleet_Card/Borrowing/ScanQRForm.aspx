﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ScanQRForm.aspx.cs" Inherits="Fleet_Card.Borrowing.ScanQRForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>

        //ไฮไลท์เมนูที่โดนเลือก และนำลิ้งค์เมนูหน้าปัจจุบันออก

        $(document).ready(function () {
            var currentmenu = document.getElementById("mainBorrowing")
            currentmenu.className = "active treeview menu-open";

            var currentmenu = document.getElementById("Borrowing")
            currentmenu.className = "active";

            var currentlink = document.getElementById("link8")
            currentlink.removeAttribute("href");

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        h3 {
            margin-top: 0px !important;
        }

        .auto-style1 {
            display: block;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            border-radius: 10px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
            box-shadow: none;
            -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            border: 1px solid #d2d6de;
            margin-top: 5px;
            padding: 6px 12px;
            background-color: #fff;
            background-image: none;
        }

        .centerimg {
            display: block;
            width: 80%;
        }
    </style>

    <asp:ScriptManager ID="ScriptManagerMainPage" runat="server"></asp:ScriptManager>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="../dist/css/skins/skin-blue.css">
    <!-- DataTables.boostrap -->
    <link rel="stylesheet" href="../bower_components/Datatable_New/DataTables-1.10.18/css/dataTables.bootstrap.css">
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="../bower_components/Datatable_New/datatables.css">
    <!-- DataTables Responsive boostrap -->
    <link rel="stylesheet" href="../bower_components/Datatable_New/Responsive-2.2.2/css/responsive.bootstrap.css" />
    <!-- DataTables Checkbox-Col -->
    <link href="../bower_components/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- jQuery-UI -->
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet" />
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.structure.css" rel="stylesheet" />
    <link href="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.theme.css" rel="stylesheet" />
    <script src="../bower_components/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <!-- jQuery-UI : Time Picker -->
    <link href="../bower_components/Time-Selection/dist/css/timepicker.css" rel="stylesheet" />
    <script src="../bower_components/Time-Selection/dist/js/timepicker.js"></script>
    <!-- Select2 -->
    <link href="../bower_components/select2/dist/css/select2.css" rel="stylesheet" />
    <script src="../bower_components/select2/dist/js/select2.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.js"></script>
    <%--    <script src="dist/js/adminlte.min.js"></script>--%>
    <!-- ChartJS NEW -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
    <!-- ChartJS Plugin -->
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels"></script>
    <!-- FastClick -->
    <%--<script src="../bower_components/fastclick/lib/fastclick.js"></script>--%>
    <!-- DataTables.boostrap -->
    <script src="../bower_components/Datatable_New/DataTables-1.10.18/js/dataTables.bootstrap.js"></script>
    <!-- DataTables -->
    <script type="text/javascript" charset="utf8" src="../bower_components/Datatable_New/datatables.js"></script>
    <!-- DataTables Responsive boostrap -->
    <script src="../bower_components/Datatable_New/Responsive-2.2.2/js/responsive.bootstrap.js"></script>
    <!-- DataTables Responsive -->
    <script src="../bower_components/Datatable_New/Responsive-2.2.2/js/dataTables.responsive.js"></script>
    <!-- DataTables checkbox-col-->
    <script src="../bower_components/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.js"></script>

    <!-- SlimScroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../bower_components/jquery-datatables-checkboxes-1.2.11/js/dataTables.checkboxes.js"></script>

    <section class="content-header">
        <%--        <div class="newStyle1">--%>
        <h3>สร้าง QR Code สำหรับยืมรถยนต์</h3>
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="form-group has-feedback col-sm-4">
                        <div>
                            หมายเลขทะเบียนรถ :
                        </div>
                        <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="80%" MaxLength="8" Visible="false"></asp:TextBox>
                        <asp:DropDownList ID="CarPlate" ClientIDMode="Static" CssClass="auto-style1" runat="server" AppendDataBoundItems="True" TabIndex="1" AutoPostBack="True" OnSelectedIndexChanged="CarPlate_SelectedIndexChanged" Width="320px">
                            <Items>
                                <asp:ListItem Text="PLACEHOLDER" Value="0" />
                            </Items>
                        </asp:DropDownList>
                        <asp:Label ID="LbCarProvince" runat="server" Text="--จังหวัด--"></asp:Label>
                    </div>

                    <div class="form-group has-feedback col-sm-4">
                        <div>
                            หมายเลขพนักงาน :
                        </div>
                        <asp:TextBox ID="TxtEmpID" runat="server" ClientIDMode="Static" CssClass="form-control" Width="80%" onkeypress="return allowOnlyNumber(event);" MaxLength="8" Enabled="false"></asp:TextBox>
                    </div>

                    <div class="form-group has-feedback col-sm-4">
                        <div>
                            ชื่อ-นามสกุล พนักงาน :
                        </div>
                        <asp:TextBox ID="TxtEmpName" runat="server" ClientIDMode="Static" CssClass="form-control" Width="80%" Enabled="false"></asp:TextBox>
                    </div>

                    <div class="form-group has-feedback col-sm-4" hidden="hidden">
                        <div>
                            เลขที่บัตรเติมน้ำมัน (Fleet Card):
                        </div>
                        <asp:TextBox ID="TxtSynergyCardNo" runat="server" CssClass="form-control" Width="264px" onkeypress="return allowOnlyNumber(event);" MaxLength="16" Enabled="false"></asp:TextBox>
                    </div>
                </div>

                <%--                <div class="form-group has-feedback col-sm-8">
                    <div class="box" style="margin-top: 20px;">
                        <div class="box-header with-border">
                            <h3 class="box-title">สถานะการขออนุมัติใช้รถยนต์เกินเวลา</h3>
                            <%--                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div> NUTCHY_COMMENT--%>
                <!-- /.box-tools -->
                <%--</div>
                <!-- /.box-header -->
                <div class="box-body">

                            <div id="divTable1" runat="server" class="box-body col-lg-12">
                                <asp:RadioButtonList ID="Rdb_BorrowType" runat="server" Width="319px" AutoPostBack="True" OnSelectedIndexChanged="Rdb_BorrowType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">ยืม-คืน: ภายใน 1 วัน</asp:ListItem>
                                    <asp:ListItem>ยืม-คืน: มากกว่า 1 วัน</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                <div class="box-body">
                            <div class="box-body">
                                <div id="div_doc" runat="server" class="box-body col-lg-12">
                                    <asp:Literal ID="DatatableToHtmlTable2" runat="server"></asp:Literal>
                                </div>
                            </div>

                            <div id="div_GvDoc" runat="server" class="box-body col-lg-12" visible="false">
                                <asp:GridView ID="Gv_Doc" runat="server"></asp:GridView>
                            </div>
                            <div id="div_Alert_NoDoc" runat="server" class="alert alert-danger alert-dismissible" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                                No document number found.
                            </div>
                        </div>
                    </div>
                </div> NUTCHY_COMMENT--%>

                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <asp:Button ID="Button1" runat="server" ClientIDMode="Static" CssClass="btn btn-success btn-block btn-flat" Width="80%" Text="Generate QR Code" OnClientClick="return checknotchoose();" OnClick="Button1_Click" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <asp:Image ID="Image1" runat="server" Visible="false" CssClass="centerimg" />
                    </div>
                </div>

                <asp:HiddenField ID="SubmitDocid" ClientIDMode="Static" runat="server" />

            </div>
        </div>
        <%--</div>--%>

        <div runat="server" visible="false">
            <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
            <asp:Button ID="btnGenerate" runat="server" Text="Generate QR Code" OnClick="btnGenerate_Click" />
            <hr />
            <asp:Image ID="imgQRCode" Width="100px" Height="100px" runat="server" Visible="false"/>
            <br />
            <br />
            <asp:Button ID="btnRead" Text="Read QR Image" runat="server" OnClick="btnRead_Click" />
            <br />
            <br />
            <asp:Label ID="lblQRCode" runat="server"></asp:Label>
        </div>
    </section>

    <script>
        function allowOnlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode < 48 || charCode > 57) {
                return false;
            }

        }

        <%--ฟังก์ชั่นเรียก Alert มีการ Fire มาจาก Code Behind (ป้องกันบัคจากากรใช้ Response.write ในการเรียก Alert)--%>
        <%--ใช้ Codebehind >>> Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "CallAlert(0);", true); --%>

        function CallAlert(typeAlert) {
            var MessageAlert;
            switch (typeAlert) {
                case 0:
                    MessageAlert = '';
                    break;
                case 1:
                    MessageAlert = '';
                    break;
                case 2:
                    MessageAlert = '';
                    break;
                case 3:
                    MessageAlert = '';
                    break;
            }
            alert(MessageAlert);
        }


        <%-- สคริปการนำ Table ด้านบนมาใส่ใน JS ของ Datatable.net --%>

        $(document).ready(function () {
            var otable = $('#exampledt2').DataTable({
                'autoWidth': false,
                'scrollY': '50vh',
                'scrollCollapse': true,
                'paging': false,
                'scrollX': true,
                'responsive': false,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                'ordering': false,
                'order': false
            });

            $('#exampledt2').css('display', 'table', 'wrap');
            $('#exampledt2').DataTable().responsive.recalc();

         <%-- สคริปการนำเลขเอกสารจากแถวที่ถูกเลือกไปเก็บไว้ใน hidden feild--%>
            $('#Button1').on('click', function () {
                var carplate = $('#CarPlate').prop('selectedIndex');
                var empid = $('#TxtEmpID').val();
                var ownername = $('#TxtEmpName').val();

                var rowselect_array = otable.column(0).checkboxes.selected();
                var select_checker = otable.column().checkboxes.selected()[0];
                var docid = "";
                if ($.trim(empid) != "" & $.trim(ownername) != "") {
                    if (typeof select_checker !== "undefined") {
                        $(rowselect_array).each(function (index, rowid) {
                            var datastring = otable.rows(rowid).data();
                            $.each(datastring, function (subindex, rowdata) {
                                docid += rowdata[1] + "/";
                            });
                        });
                        var showdocid = docid.split("/").join("\n")

                        var c = confirm("คุณต้องการใช้เอกสารหมายเลข\n" + showdocid + "ในการสร้าง QR Code การยืมบัตร Fleet Card ใช่หรือไม่")
                        if (c) {
                            $('#SubmitDocid').val(docid);
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else {
                        alert("กรุณาเลือกเอกสารที่ต้องการอนุมัติก่อนทำการกดปุ่มอนุมัติ")
                        return false;
                    }
                }
                else if ($.trim(empid) == "") {
                    return false;
                }

                else if ($.trim(ownername) == "") {
                    return false;
                }

            });

        });

         <%--ฟังก์ชั่นเช็คช่องข้อมูลที่ยังไม่กรอกตอนกด confrim --%>
        function checknotchoose() {
            var carplate = $('#CarPlate').prop('selectedIndex');

            if (carplate > 0) {

                var empid = $('#TxtEmpID').val();
                var ownername = $('#TxtEmpName').val();

                if ($.trim(empid) != "" & $.trim(ownername) != "") {
                    return true;
                }
                else if ($.trim(empid) == "") {
                    alert('กรุณากรอกรหัสพนักงาน');
                    $('#TxtEmpID').focus();
                    $('#TxtEmpID').focus(function () {
                        $(this).css("border", "#ff3300 solid 3px");
                    });
                    $('#TxtEmpID').blur(function () {
                        $(this).css("border", "#999999 solid 1px");
                    });
                    return false;
                }

                else if ($.trim(ownername) == "") {
                    alert('กรุณากรอกชื่อพนักงาน');
                    $('#TxtEmpName').focus();
                    $('#TxtEmpName').focus(function () {
                        $(this).css("border", "#ff3300 solid 3px");
                    });
                    $('#TxtEmpName').blur(function () {
                        $(this).css("border", "#999999 solid 1px");
                    });
                    return false;
                }

            }
            else if (carplate <= 0) {
                alert('กรุณาเลือกทะเบียนรถที่ท่านต้องการจะยืม');
                $('#CarPlate').select2('open');
                return false;
            }
            else {
                alert('ERROR: Clicking Java Failed, Contact admin for further information and help.')
                return false;
            }
        }


        //$('#RejectBtn').on('click', function () {
        //    var rowselect_array = otable.column(0).checkboxes.selected();
        //    var select_checker = otable.column().checkboxes.selected()[0];
        //    var docid = "";
        //    if (typeof select_checker !== "undefined") {
        //        $(rowselect_array).each(function (index, rowid) {
        //            var datastring = otable.rows(rowid).data();
        //            $.each(datastring, function (subindex, rowdata) {
        //                docid += rowdata[1] + "/";
        //            });
        //        });
        //        var showdocid = docid.split("/").join("\n")
        //        var c = confirm("คุณต้องการ-'ไม่'-อนุมัติเอกสาร หมายเลข\n" + showdocid)
        //        if (c) {
        //            $('#RejectDocid').val(docid);
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //    else {
        //        alert("กรุณาเลือกเอกสารที่ต้องการอนุมัติก่อนทำการกดปุ่มไม่อนุมัติ")
        //        return false;
        //    }
        //});

        $(function () {

            $("#CarPlate").select2({
                placeholder: {
                    id: '0',
                    text: 'เลือกทะเบียนรถ',
                },
                allowClear: true,
                width: '80%'

            });
        });

    </script>

</asp:Content>
