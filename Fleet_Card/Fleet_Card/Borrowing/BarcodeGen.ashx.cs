﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ZXing;
using ZXing.QrCode;

namespace Fleet_Card.Borrowing
{
    /// <summary>
    /// Summary description for BarcodeGen
    /// </summary>
    public class BarcodeGen : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //Get value to encode from querystring
            string EmpID = context.Request.QueryString["EmpID"].ToString(); 
            string OilCardNo = context.Request.QueryString["CardNo"].ToString();
            string CarNo = context.Request.QueryString["CarNo"].ToString();
            string Doc_id = context.Request.QueryString["Doc_id"].ToString();
            //string valueToEncode = CarNo + "\n-ส่งคืนบัตร:" + OilCardNo + "\nเวลา:" + DateTime.Now.ToString();

            //string valueToEncode = "R|\n14|\nออนเทรดหัวหิน|\n7|\n12.59537|\n99.94524";
            string valueToEncode = "B|\n" + EmpID + "|\n" + CarNo + "|\n" + OilCardNo + "|\n" + Doc_id + "|\n" + DateTime.Now.ToString();

            //string valueToEncode = CarNo + "|" + OilCardNo;
            //string valueToEncode = CarNo + OilCardNo;          

            QrCodeEncodingOptions options = new QrCodeEncodingOptions();
            options = new QrCodeEncodingOptions
            {
                DisableECI = true,
                CharacterSet = "UTF-8",
                Width = 250,
                Height = 250,
            };

            var writer = new BarcodeWriter();
            writer.Format = BarcodeFormat.QR_CODE;
            writer.Options = options;
            //var result = writer.Write("http://www.dotnetthoughts.net");
            var result = writer.Write(valueToEncode);
            var barcodeBitmap = new Bitmap(result);
            barcodeBitmap.Save
                (context.Response.OutputStream, ImageFormat.Jpeg);
            context.Response.ContentType = "image/jpeg";
            context.Response.End();
        }

        public void ProcessRequest2(HttpContext context)
        {
            var QrcodeContent = "AAAA"; //context.AllAttributes["content"].Value.ToString();
            var alt = "QR Code"; //context.AllAttributes["alt"].Value.ToString();
            var width = 250; // width of the Qr Code
            var height = 250; // height of the Qr Code
            var margin = 0;

            var qrCodeWriter = new ZXing.BarcodeWriterPixelData
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions { Height = height, Width = width, Margin = margin }
            };

            var pixelData = qrCodeWriter.Write(QrcodeContent);

            // creating a bitmap from the raw pixel data; if only black and white colors are used it makes no difference
            // that the pixel data ist BGRA oriented and the bitmap is initialized with RGB
            using (var bitmap = new System.Drawing.Bitmap(pixelData.Width, pixelData.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb))
            using (var ms = new MemoryStream())
            {

                var bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, pixelData.Width, pixelData.Height),
                   System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                try
                {
                    // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image
                    System.Runtime.InteropServices.Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0,
                       pixelData.Pixels.Length);
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }
                // save to stream as PNG
                bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                //output.TagName = "img";
                //output.Attributes.Clear();
                //output.Attributes.Add("width", width);
                //output.Attributes.Add("height", height);
                //output.Attributes.Add("alt", alt);
                //output.Attributes.Add("src",
                //String.Format("data:image/png;base64,{0}", Convert.ToBase64String(ms.ToArray())));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}