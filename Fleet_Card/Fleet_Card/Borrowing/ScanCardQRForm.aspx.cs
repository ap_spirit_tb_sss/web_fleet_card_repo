﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ZXing;
using ZXing.QrCode;
using System.Configuration;

using System.Web.Script.Serialization;

namespace Fleet_Card.Borrowing
{
    public partial class ScanCardQRForm : System.Web.UI.Page
    {
        public CommonLibrary.Collection.CollectionHelper CommonObj;
        string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
        SqlDataAdapter dtAdapter;
        string strSQL;
        SqlDataReader readersql;
        SqlConnection objConn = new SqlConnection();
        SqlCommand objCmd = new SqlCommand();
        string EmpID_Login = string.Empty;
        DataTable dtProvince = new DataTable(), dtProToAmp = new DataTable(), dtProToDist = new DataTable(), dtRegion = new DataTable(), dtAllcar = new DataTable();
        string EmpID = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                EmpID = Session["EmpID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('Error: change to login-page');window.location = 'Login.aspx';</script>");
            }


            if (!IsPostBack)
            {
                //GET_Doc_Cars_Wolkflow(EmpID, "X", -1);
                Get_CarPlate();
                TxtEmpID.Attributes.Add("ReadOnly", "true");
                TxtEmpName.Attributes.Add("ReadOnly", "true");
                TxtSynergyCardNo.Attributes.Add("ReadOnly", "true");
                Rdb_BorrowType.SelectedIndex = -1;
            }
            else
            {
                ////Job_Status = int.Parse(Request.QueryString["values"].ToString());
                //GET_Menu_RolePermission(Userlogin, User_RoleName, strPage);
                //Get_RequestHistory();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //DataTable DT_Doc = (DataTable)ViewState["DT_Doc"];
            //HtmlTable_Doc.Text = Poolfunc.ConvertDataTableToHTMLApprove(DT_Doc, 1);
            string Doc_id_Lists = SubmitDocid.Value.ToString();

            if (this.CarPlate.SelectedItem.Text.Length > 0)
            {
                this.Image1.ImageUrl = "~/Borrowing/BarcodeCardGen.ashx?EmpID=" + TxtEmpID.Text + "&CarNo=" + this.CarPlate.SelectedItem.Text + "&CardNo=" + TxtSynergyCardNo.Text + "&Doc_id=" + Doc_id_Lists + "&BR_Type=" + Rdb_BorrowReturn.SelectedValue.ToString() + "&DM_Type=" + Rdb_BorrowType.SelectedValue.ToString();
                this.Image1.Visible = true;
            }

        }

        //protected void btnGenerate_Click(object sender, EventArgs e)
        //{
        //    GenerateCode(txtCode.Text);
        //}

        //protected void btnRead_Click(object sender, EventArgs e)
        //{
        //    ReadQRCode();
        //}

        //// Generate QRCode
        //private void GenerateCode(string name)
        //{
        //    //var writer = new BarcodeWriter();
        //    //writer.Format = BarcodeFormat.QR_CODE;
        //    //var result = writer.Write(name);
        //    //string path = Server.MapPath("~/images/QRImage.jpg");
        //    //var barcodeBitmap = new Bitmap(result);

        //    //using (MemoryStream memory = new MemoryStream())
        //    //{
        //    //    using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
        //    //    {
        //    //        barcodeBitmap.Save(memory, ImageFormat.Jpeg);
        //    //        byte[] bytes = memory.ToArray();
        //    //        fs.Write(bytes, 0, bytes.Length);
        //    //    }
        //    //}
        //    //imgQRCode.Visible = true;
        //    //imgQRCode.ImageUrl = "~/images/QRImage.jpg";

        //}

        //private void ReadQRCode()
        //{
        //    //var reader = new QRCodeReader();
        //    //string filename = Path.Combine(Request.MapPath("~/images"), "QRImage.jpg");
        //    //// Detect and decode the barcode inside the bitmap
        //    //var result = reader.Decode(new Bitmap(filename));
        //    //if (result != null)
        //    //{
        //    //    lblQRCode.Text = "QR Code: " + result.Text;
        //    //}
        //}

        //[WebMethod]
        //public static string GetPiechartData()
        //{
        //    string strCon = System.Configuration.ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString;
        //    using (SqlConnection con = new SqlConnection(strCon))
        //    {
        //        SqlCommand cmd = new SqlCommand("GET_SumBudgetByComp_Grah", con);
        //        cmd.CommandType = CommandType.StoredProcedure;

        //        //cmd.Parameters.Add("@BudgetType", SqlDbType.Int);
        //        //cmd.Parameters[0].Value = -1;  //1: ASSET | 2: EXPANE
        //        //cmd.Parameters[0].Direction = ParameterDirection.Input;

        //        //cmd.Parameters.Add("@BudgetYear", SqlDbType.NVarChar);
        //        //cmd.Parameters[1].Value = "XXX";  //2019
        //        //cmd.Parameters[1].Direction = ParameterDirection.Input;

        //        //cmd.Parameters.Add("@BudgetCode", SqlDbType.NVarChar);
        //        //cmd.Parameters[2].Value = "XXX";  //ASS_2019_R1_NUM
        //        //cmd.Parameters[2].Direction = ParameterDirection.Input;

        //        //cmd.Parameters.Add("@RegionCode", SqlDbType.NVarChar);
        //        //cmd.Parameters[3].Value = "XXX";  //R1 : ภาค 1
        //        //cmd.Parameters[3].Direction = ParameterDirection.Input;

        //        //cmd.Parameters.Add("@CompCode", SqlDbType.NVarChar);
        //        //cmd.Parameters[4].Value = "XXX";  //NY : นำยุุค จำกัด
        //        //cmd.Parameters[4].Direction = ParameterDirection.Input;

        //        //cmd.Parameters.Add("@EmpNo", SqlDbType.NVarChar);
        //        //cmd.Parameters[5].Value = "i1005653";  //NY : นำยุุค จำกัด
        //        //cmd.Parameters[5].Direction = ParameterDirection.Input;                


        //        //string sql = "SELECT [BudgetYear],[RegCode],[CompCode],[Status],[IsCurrent], Comp.CompNameTH,Sum(Detail.NetValue) as NetValue " +
        //        //    "FROM[HCBudget].[dbo].[Tbt_BudgetMain] Main " +
        //        //    "INNER JOIN[dbo].[Tbt_BudgetAssetDetail] Detail " +
        //        //    "ON Main.ID = Detail.RId " +
        //        //    "INNER JOIN[dbo].[Tbm_Company] Comp " +
        //        //    "ON Main.CompCode = Comp.CompID " +
        //        //    "Where Main.IsCurrent= 1 and Status = 5 and BudgetType = 1 " +
        //        //    "GROUP BY Main.BudgetYear,Main.RegCode,main.CompCode,Main.Status,Main.IsCurrent,Comp.CompNameTH";

        //        //SqlCommand cmd = new SqlCommand(sql, con);
        //        //cmd.CommandText = sql;
        //        ////cmd.Parameters.Add("@legal_GUID", SqlDbType.UniqueIdentifier).SqlValue = legal_GUID;


        //        SqlDataAdapter da = new SqlDataAdapter();
        //        da.SelectCommand = cmd;
        //        DataTable dt = new DataTable();
        //        da.Fill(dt);

        //        List<GraphData> dataList = new List<GraphData>();

        //        foreach (DataRow dtrow in dt.Rows)
        //        {
        //            GraphData details = new GraphData();
        //            details.label = dtrow[0].ToString();
        //            if (dtrow["R1_N"].ToString() != "") { details.R1_N = double.Parse(dtrow["R1_N"].ToString()); } else { details.R1_N = 0; }
        //            if (dtrow["R2_N"].ToString() != "") { details.R2_N = double.Parse(dtrow["R2_N"].ToString()); } else { details.R2_N = 0; }
        //            if (dtrow["R3_N"].ToString() != "") { details.R3_N = double.Parse(dtrow["R3_N"].ToString()); } else { details.R3_N = 0; }

        //            dataList.Add(details);
        //        }

        //        //oper = null which means its first load.
        //        var jsonSerializer = new JavaScriptSerializer();
        //        string data = jsonSerializer.Serialize(dataList);
        //        //data = [{"label":"สนญ.","test1":55,"test2":55,"test3":42},{ "label":"R1-นำ","test1":113,"test2":113,"test3":104},{ "label":"R1-ป้อม","test1":77,"test2":76,"test3":77}];
        //        //string data = [{ "label":"2018","value":1000},{ "label":"2019","value":1000},{ "label":"2020","value":1000}];
        //        return data;
        //    }
        //}

        protected void CarPlate_SelectedIndexChanged(object sender, EventArgs e)
        {
            Get_CarPlate_Detail(CarPlate.SelectedItem.Text);
        }

        public class GraphData
        {
            //public DateTime Date { get; set; }
            public string label { get; set; }
            public double R1_N { get; set; }
            public double R2_N { get; set; }
            public double R3_N { get; set; }
        }


        //protected void Rdb_BorrowType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (Rdb_BorrowType.SelectedIndex == 0)
        //    {
        //        div_doc.Visible = false;
        //        div_GvDoc.Visible = false;
        //        div_Alert_NoDoc.Visible = false;
        //    }
        //    else if (Rdb_BorrowType.SelectedIndex == 1)
        //    {
        //        GET_Doc_Cars_Wolkflow(EmpID, "X", -1);
        //    }
        //}


        //public void GET_Doc_Cars_Wolkflow(string EmpId_Login, string Document_id, int SysStatus)
        //{
        //    DataSet dsresult = new DataSet();
        //    DataSet ds = new DataSet();
        //    DataTable DT2 = new DataTable(), dtShow = new DataTable();

        //    var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;

        //    string strSql = @"[dbo].[GET_Doc_Cars_Wolkflow]";

        //    SqlConnection conn = new SqlConnection(strConnString);
        //    conn.Open();
        //    SqlCommand dCmd = new SqlCommand(strSql, conn);
        //    dCmd.CommandType = CommandType.StoredProcedure;

        //    try
        //    {

        //        dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
        //        dCmd.Parameters[0].Value = EmpId_Login;  //2019
        //        dCmd.Parameters[0].Direction = ParameterDirection.Input;

        //        dCmd.Parameters.Add("@Document_id", SqlDbType.NVarChar);
        //        dCmd.Parameters[1].Value = Document_id;  //ASS_2019_R1_NUM
        //        dCmd.Parameters[1].Direction = ParameterDirection.Input;

        //        dCmd.Parameters.Add("@Status", SqlDbType.Int);
        //        dCmd.Parameters[2].Value = SysStatus;  //NY : นำยุุค จำกัด
        //        dCmd.Parameters[2].Direction = ParameterDirection.Input;

        //        var adapter = new SqlDataAdapter(dCmd);

        //        adapter.Fill(DT2);

        //        //return dsresult;
        //        //DT2.Columns.Add("check", typeof(System.Int32));
        //        //foreach (DataRow row in DT2.Rows)
        //        //{
        //        //    //need to set value to NewColumn column
        //        //    row["check"] = 0;   // or set it to some other value
        //        //}


        //        //DataColumn Col = DT2.Columns.Add("เลือก");
        //        //Col.SetOrdinal(0);// to put the column in position 0;
        //        //foreach (DataRow row in DT2.Rows)
        //        //{
        //        //    //need to set value to NewColumn column
        //        //    row["เลือก"] = 0;   // or set it to some other value
        //        //
        //        //} >>> Nut_6/9/18


        //        if (DT2.Rows.Count > 0)
        //        {
        //            int i = 0, j = 0;

        //            DT2.Columns[0].ColumnName = "เลขที่เอกสาร";
        //            DT2.Columns[3].ColumnName = "รหัสพนักงาน";
        //            DT2.Columns[4].ColumnName = "วันเวลาที่ยืม";
        //            DT2.Columns[5].ColumnName = "วันเวลาที่จะคืน";
        //            DT2.Columns.RemoveAt(1);
        //            DT2.Columns.RemoveAt(1);

        //            DT2.Columns.Add("Select");
        //            DT2.Columns["Select"].SetOrdinal(0);

        //            for (i = 0; i < DT2.Rows.Count; i++)
        //            {
        //                for (j = 0; j < DT2.Columns.Count; j++)
        //                {
        //                    if (j == 0)
        //                    {
        //                        DT2.Rows[i][j] = i;
        //                    }
        //                }
        //            }
        //            //Gv_Doc.DataSource = DT2;
        //            //Gv_Doc.DataBind();
        //            //div_GvDoc.Visible = true; div_Alert_NoDoc.Visible = false;
        //            div_doc.Visible = true; div_Alert_NoDoc.Visible = false;

        //            DatatableToHtmlTable2.Text = Poolfunc.ConvertDataTableToHTMLApprove(DT2, 2);
        //            //ViewState["DT_Doc"] = DT;
        //        }
        //        else
        //        {
        //            //Gv_Doc.DataSource = null;
        //            //Gv_Doc.DataBind();
        //            //div_GvDoc.Visible = false; div_Alert_NoDoc.Visible = true;
        //            div_doc.Visible = false; div_Alert_NoDoc.Visible = true;
        //        }

        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        dCmd.Dispose();
        //        conn.Close();
        //        conn.Dispose();
        //    }

        //}

        public void Get_CarPlate()
        {
            var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            objConn.ConnectionString = strConnString;
            objConn.Open();

            //GET_Asset_Data();

            DataTable dtAllcar = new DataTable();
            int Region = int.Parse(Session["Region"].ToString());

            strSQL = "Select * from View_CarBrand where Region = " + Region;
            dtAdapter = new SqlDataAdapter(strSQL, objConn);
            dtAdapter.Fill(dtAllcar);

            ViewState["dtAllcar"] = dtAllcar;
            CarPlate.DataSource = dtAllcar;
            CarPlate.DataTextField = "LicensePlate_Car";
            CarPlate.DataValueField = "CarID";
            CarPlate.DataBind();

            objConn.Close();
        }

        public void Get_CarPlate_Detail(string CarPlate)
        {
            var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            objConn.ConnectionString = strConnString;
            objConn.Open();

            //GET_Asset_Data();

            DataTable dtAllcar = new DataTable();
            int Region = int.Parse(Session["Region"].ToString());

            strSQL = "Select * from View_CarBrand where [LicensePlate_Car] = '" + CarPlate + "'";
            dtAdapter = new SqlDataAdapter(strSQL, objConn);
            dtAdapter.Fill(dtAllcar);

            if (dtAllcar.Rows.Count > 0)
            {
                TxtEmpID.Attributes.Remove("ReadOnly");
                TxtEmpName.Attributes.Remove("ReadOnly");
                TxtEmpID.Text = dtAllcar.Rows[0]["EmpID"].ToString();
                TxtEmpName.Text = dtAllcar.Rows[0]["EmpName"].ToString();
                TxtSynergyCardNo.Text = dtAllcar.Rows[0]["FleetCardNo"].ToString();
                LbCarProvince.Text = dtAllcar.Rows[0]["Province"].ToString();
            }
            else
            {
                TxtEmpID.Text = "";
                TxtEmpName.Text = "";
                TxtSynergyCardNo.Text = "";
                LbCarProvince.Text = "";
                TxtEmpID.Attributes.Add("ReadOnly", "true");
                TxtEmpName.Attributes.Add("ReadOnly", "true");
            }

            objConn.Close();
        }

    }
}