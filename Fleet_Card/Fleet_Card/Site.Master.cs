﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fleet_Card
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string EmpID_Login = string.Empty;

            // เช็คล๊อคอิน
            try
            {
                if (Session["Token"].ToString() == "" || Session["Token"].ToString() == null)
                {
                    Response.Redirect("~/Login.aspx");
                }

                EmpID_Login = Session["EmpID"].ToString();

                int Approve_Count = 0;
                int Reject_Count = 0;

                if (!Page.IsPostBack)
                {
                    //------------ Theerayut.S : Add for role permission menu access ---------------------------------******
                    Control MainMenu = this.Page.Master.FindControl("MainMenu");
                    this.Page.Master.FindControl("MainMenu").FindControl("mainApprove").Visible = true;


                    //------------ NUTCHAPON.M : Replace form MainPage > Notify Number of Document < ---------------------------------******
                    DataTable dtFormSQL = Poolfunc.GET_Doc_PendingApprove_ByUser("X", EmpID_Login, "X", -1, -1);
                    DataView dv_count_All = new DataView(dtFormSQL);

                    Session["All_Request"] = dv_count_All.Count; //All request by the user

                    dv_count_All.RowFilter = "Status = 6";  //Approved status
                    Approve_Count = dv_count_All.Count;
                    Session["All_Approve"] = Approve_Count;

                    dv_count_All.RowFilter = "Status = 5";  //Deny status
                    Reject_Count = dv_count_All.Count;
                    Session["All_Reject"] = Reject_Count;

                    //Control Menu = this.Page.Master.FindControl("MainMenu").FindControl("mainApprove");
                    //Menu.Visible = true;
                }

                //else
                //{
                //    DataTable ADtable = Session["ADTable"] as DataTable;
                //    if (Session["FristTime"].ToString() == "0") { 
                //        Session["EmpID"] = ADtable.Rows[0][1].ToString();
                //        Session["EmpEmail"] = ADtable.Rows[0][2].ToString();
                //        Session["Name"] = ADtable.Rows[0][5].ToString();
                //        Session["LastName"] = ADtable.Rows[0][6].ToString();
                //        Session["EmpFullName"] = ADtable.Rows[0][5].ToString() + "  " + ADtable.Rows[0][6].ToString();
                //        Session["Company"] = ADtable.Rows[0][9].ToString();
                //        Session["Org"] = ADtable.Rows[0][10].ToString();
                //        Session["EmpPosition"] = ADtable.Rows[0][11].ToString();
                //        Session["Workplace"] = ADtable.Rows[0][12].ToString();
                //        Session["FristTime"] = 1;
                //    }

            }

            catch (Exception ex)
            {
                Response.Write("<script> alert('Error :: '" + ex.Message + "'')</script>");
                Response.Redirect("~/Login.aspx");
            }
        }

        protected void Logout_click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
}