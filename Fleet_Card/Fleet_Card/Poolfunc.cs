﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Windows;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using OfficeOpenXml.Style;
using System.Xml;
using System.IO;
using System.Web.Services;
using System.Web.Script;
using System.Web.Script.Services;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Data;

namespace Fleet_Card
{
    public static class Poolfunc
    {
        public static int Find_Region(int CompID)
        {
            DataTable dtRegion = new DataTable();
            DataTable dtAllcarRep = new DataTable();
            int Region = 0;

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            SqlDataAdapter dtAdapter;
            string strSQL;
            SqlConnection objConn = new SqlConnection();
            SqlCommand objCmd = new SqlCommand();
            string EmpID_Login = string.Empty;


            var strConnString = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            objConn.ConnectionString = strConnString;
            objConn.Open();

            strSQL = "Select * from Company where CompCode = " + CompID;
            dtAdapter = new SqlDataAdapter(strSQL, objConn);
            dtAdapter.Fill(dtRegion);

            if (dtRegion.Rows.Count > 0)
            {
                Region = int.Parse(dtRegion.Rows[0]["Region"].ToString());
            }

            objConn.Close();
            objCmd.Dispose();

            return Region;
        }

        public static Tuple<string, string, int> CREATE_CMS_Wolkflow(string EmpId_Login, int Process_ID, string cmsDetailxml)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            string EmpID_Login = "";
            string Doc_id = "";
            string EmpID_Current = "";
            int dsresult = 0;
            DataSet ds = new DataSet();
            string strSql = @"[dbo].[CREATE_CMS_Wolkflow]";

            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand dCmd = new SqlCommand(strSql, conn);
            dCmd.CommandType = CommandType.StoredProcedure;

            try
            {
                dCmd.Parameters.Add("@Process_ID", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = Process_ID;
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@UserID_Login", SqlDbType.Int);
                dCmd.Parameters[1].Value = EmpId_Login;
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@WF_Detail", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = cmsDetailxml;
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                //dCmd.Parameters.Add("@Current_EmpID", SqlDbType.NVarChar); //Return EmpID who the next current Approver step
                //dCmd.Parameters[3].Value = EmpID_Current;
                //dCmd.Parameters[3].Direction = ParameterDirection.Output;

                //dCmd.Parameters.Add("@Doc_ID", SqlDbType.NVarChar);
                //dCmd.Parameters[4].Value = Doc_id;
                //dCmd.Parameters[4].Direction = ParameterDirection.Output;

                //dCmd.Parameters.Add("@Result", SqlDbType.Int);
                //dCmd.Parameters[5].Value = dsresult;
                //dCmd.Parameters[5].Direction = ParameterDirection.Output;

                SqlParameter EmpIDCurrent = new SqlParameter("@Current_EmpID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(EmpIDCurrent);
                SqlParameter Docid = new SqlParameter("@Doc_ID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(Docid);
                SqlParameter Output = new SqlParameter("@Result", SqlDbType.Int, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(Output);


                dCmd.ExecuteNonQuery();

                // read output value from @Result
                //dsresult = Convert.ToInt32(dCmd.Parameters["@Result"].Value);

                EmpID_Current = EmpIDCurrent.Value.ToString();
                Doc_id = Docid.Value.ToString();
                dsresult = Convert.ToInt32(Output.Value.ToString());

                return new Tuple<string, string, int>(EmpID_Current, Doc_id, dsresult);

                //return Doc_id;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }

        public static Tuple<string, string, int> Approved_All_Wolkflow(string EmpId_Login, string DocID_Lists,int IsApproved)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            string EmpID_Login = "";
            string Doc_id = "";
            string EmpID_Current = "";
            int dsresult = 0;
            DataSet ds = new DataSet();
            string strSql = @"[dbo].[Approved_All_Wolkflow]";

            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            SqlCommand dCmd = new SqlCommand(strSql, conn);
            dCmd.CommandType = CommandType.StoredProcedure;

            try
            {

                dCmd.Parameters.Add("@UserID_Login", SqlDbType.Int);
                dCmd.Parameters[0].Value = EmpId_Login;
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Document_id_List", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = DocID_Lists;
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@IsApprove", SqlDbType.Int);
                dCmd.Parameters[2].Value = IsApproved;
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                SqlParameter EmpID_Requester = new SqlParameter("@EmpID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(EmpID_Requester);
                SqlParameter EmpIDCurrent = new SqlParameter("@Current_EmpID", SqlDbType.NVarChar, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(EmpIDCurrent);
                SqlParameter Output = new SqlParameter("@Result", SqlDbType.Int, 100) { Direction = ParameterDirection.Output };
                dCmd.Parameters.Add(Output);


                dCmd.ExecuteNonQuery();

                // read output value from @Result
                //dsresult = Convert.ToInt32(dCmd.Parameters["@Result"].Value);

                EmpID_Login = EmpID_Requester.Value.ToString();
                EmpID_Current = EmpIDCurrent.Value.ToString();
                dsresult = int.Parse(Output.Value.ToString());

                return new Tuple<string, string, int>(EmpID_Current, Doc_id, dsresult);

                //return Doc_id;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
        }

        public static DataTable GET_Doc_PendingApprove_ByUser(string EmpID_Approver, string EmpID_Requestor, string Document_id, int Status, int ProcessID)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Doc_PendingApprove_ByUser]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Approver", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Approver;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID_Requestor", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Requestor;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Document_id", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = Document_id;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Status", SqlDbType.Int);
                dCmd.Parameters[3].Value = Status;  //ASS_2019_R1_NUM
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@ProcessID", SqlDbType.Int);
                dCmd.Parameters[4].Value = ProcessID;  //R1 : ภาค 1
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

                //if (DT.Rows.Count > 0)  //(ViewState["Assets_MainData"] == null && DT.Rows.Count > 0)
                //{
                //    //ViewState["Assets_MainData"] = DT;

                //    //GV_AssetDetail.DataSource = DT;
                //    //GV_AssetDetail.DataBind();
                //}
                //else
                //{
                //    if (DT.Rows.Count > 0)
                //    {

                //        //GV_AssetDetail.DataSource = DT;
                //        //GV_AssetDetail.DataBind();
                //    }
                //    else
                //    {
                //        //GV_AssetDetail.DataSource = null;
                //        //GV_AssetDetail.DataBind();
                //    }
                //}
            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        public static string ConvertDataTableToHTMLMain(DataTable dtItems, int number)
        {
            string dtname = "exampledt" + number + "";
            string html = "<table id='" + dtname + "' runat='server' class='table table-bordered table-hover' style='width:auto;height:auto;table-layout:fixed;'>";
            //add column and header
            html += "<thead><tr>";
            for (int i = 0; i < dtItems.Columns.Count; i++)
            {
                html += "<th>" + dtItems.Columns[i].ColumnName + "</th>";
            }
            html += "</tr></thead>";
            //add rows
            html += "<tbody>";
            for (int i = 0; i < dtItems.Rows.Count; i++)
            {
                html += "<tr style='min-height:80px;max-height: 80px;'>";
                for (int j = 0; j < dtItems.Columns.Count; j++)
                {

                    //if (j == 6)
                    //{
                    //    html += "<td style='text-align: left;'><div style='word-wrap: break-word;'> " + dtItems.Rows[i][j].ToString() + "</div></td>";
                    //}
                    //else
                    //{
                    html += "<td>" + dtItems.Rows[i][j].ToString() + "</td>";
                    //}
                }
                html += "</tr>";
            }
            html += "</tbody>";
            html += "</table>";
            return html;
        }

        public static string ConvertDataTableToHTMLApprove(DataTable dtItems, int number)
        {
            string dtname = "exampledt" + number + "";
            string html = "<table id='" + dtname + "' runat='server' class='table table-bordered table-hover' style='width:auto;height:auto;table-layout:fixed;'>";
            //add column and header
            html += "<thead><tr>";
            for (int i = 0; i < dtItems.Columns.Count; i++)
            {
                if (i == 0)
                {
                    html += "<th></th>";
                }
                else
                {
                    html += "<th>" + dtItems.Columns[i].ColumnName + "</th>";
                }

            }
            html += "</tr></thead>";
            //add rows
            html += "<tbody>";
            for (int i = 0; i < dtItems.Rows.Count; i++)
            {
                html += "<tr style='min-height:80px;max-height: 80px;'>";
                for (int j = 0; j < dtItems.Columns.Count; j++)
                {
                    //if (j == 0) {
                    //    html += "<td></td>";

                    //}
                    //else
                    //{
                    html += "<td>" + dtItems.Rows[i][j].ToString() + "</td>";
                    //}
                }
                html += "</tr>";
            }
            html += "</tbody>";
            html += "</table>";
            return html;
        }

        public static DataTable FilterDTMain(DataTable dtSQL, int type, Page requetpage)
        {
            //DataTable test = LoginThaiBev.EmpInfo.GetEmployeeINFO("11034774");
            string EmpID_Login = string.Empty;
            DataRow dr1, dr2;
            int i = 0, j = 0;
            DataTable dt = new DataTable();

            dt.Columns.Add("สถานะ", typeof(String));
            dt.Columns.Add("เลขที่เอกสาร", typeof(String));
            dt.Columns.Add("ผู้อนุมัติปัจุบัน", typeof(String));
            dt.Columns.Add("ทะเบียนรถ", typeof(String));
            dt.Columns.Add("ยี่ห้อ", typeof(String));
            dt.Columns.Add("วงเงินตามการ์ด(ลิตร)", typeof(String));
            dt.Columns.Add("ขอเงินสดจำนวน(บาท)", typeof(String));
            dt.Columns.Add("น้ำมันจำนวน(ลิตร)", typeof(String));
            dt.Columns.Add("วัน/เวลาที่ยืม", typeof(String));
            dt.Columns.Add("วัน/เวลาที่คืน", typeof(String));
            dt.Columns.Add("สถานที่จอดรถ", typeof(String));  //proces_id = 1 : By theerayut.s 
            dt.Columns.Add("เหตุผลในการยืม", typeof(String));
            //dt.Columns.Add("สถานที่จอดรถ", typeof(String));  //proces_id = 1 : By theerayut.s 

            if (dtSQL.Rows.Count > 0)
            {


                for (i = 0; i < dtSQL.Rows.Count; i++)

                {

                    dr1 = dt.NewRow();
                    dt.Rows.Add(dr1);

                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 0)
                        {
                            if (dtSQL.Rows[i]["Status"].ToString() == "2")
                            {
                                dt.Rows[i][j] = "<img src='Images/Waited_mini_20px.png' /><div style='display: none; '>Waiting,รอตรวจ</div>";
                            }
                            else if (dtSQL.Rows[i]["Status"].ToString() == "6")
                            {
                                dt.Rows[i][j] = "<img src='Images/Check_mini_20px.png' /><div style='display: none; '>Approved,ยืนยัน,ผ่าน,อนุมัติ</div>";
                            }
                            else
                            {
                                dt.Rows[i][j] = "<img src='Images/Reject_mini_20px.png' /><div style='display: none; '>Denied,Rejected,ยกเลิก</div>";
                            }
                        }
                        else if (j == 1)
                        {
                            if (dtSQL.Rows[i]["document_id"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["document_id"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 2)
                        {
                            if (dtSQL.Rows[i]["Approver_By"].ToString() != "0" && dtSQL.Rows[i]["Approver_By"].ToString() != "")
                            {
                                DataTable ApproverInfo = LoginThaiBev.EmpInfo.GetEmployeeINFO(dtSQL.Rows[i]["Approver_By"].ToString());
                                string ApproverName = ApproverInfo.Rows[0]["LocalFirstName"].ToString() + "  " + ApproverInfo.Rows[0]["localLastName"].ToString();
                                dt.Rows[i][j] = ApproverName;
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 3)
                        {
                            if (dtSQL.Rows[i]["Plate_Car"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Plate_Car"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 4)
                        {
                            if (dtSQL.Rows[i]["Band_Car"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Band_Car"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 5)
                        {
                            if (dtSQL.Rows[i]["Oil_Limit"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_Limit"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 6)
                        {
                            if (dtSQL.Rows[i]["Oil_Cash"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_Cash"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 7)
                        {
                            if (dtSQL.Rows[i]["Oil_litre"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_litre"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 8 || j == 9)
                        {
                            if (dtSQL.Rows[i]["Start_Date"].ToString() != "")
                            {
                                if (type != 3)
                                {
                                    if (dtSQL.Rows[i]["EndTime"].ToString() != "" && dtSQL.Rows[i]["End_Date"].ToString() != "")
                                    {
                                        if (j == 8)
                                        {
                                            DateTime fullstarttime = DateTime.Parse(dtSQL.Rows[i]["Start_Date"].ToString()).Date.Add(DateTime.Parse(dtSQL.Rows[i]["StartTime"].ToString()).TimeOfDay);

                                            dt.Rows[i][j] = fullstarttime.ToString("dd/MM/yyyy hh:mm");
                                        }
                                        else
                                        {
                                            DateTime fullendtime = DateTime.Parse(dtSQL.Rows[i]["End_Date"].ToString()).Date.Add(DateTime.Parse(dtSQL.Rows[i]["EndTime"].ToString()).TimeOfDay);

                                            dt.Rows[i][j] = fullendtime.ToString("dd/MM/yyyy hh:mm");
                                        }
                                    }
                                    else
                                    {
                                        dt.Rows[i][j] = "";
                                        requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                        break;
                                    }
                                }
                                else
                                {
                                    if (j == 8)
                                    {
                                        dt.Rows[i][j] = DateTime.Parse(dtSQL.Rows[i]["Start_Date"].ToString()).ToString("dd/MM/yyy");
                                    }
                                    else
                                    {
                                        dt.Rows[i][j] = "";
                                    }
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }

                        }
                        else if (j == 10)
                        {
                            if (dtSQL.Rows[i]["Parking_Address"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Parking_Address"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else
                        {
                            if (type != 1)
                            {
                                if (dtSQL.Rows[i]["Reason"].ToString() != "")
                                {
                                    dt.Rows[i][j] = dtSQL.Rows[i]["Reason"].ToString();
                                }
                                else
                                {
                                    dt.Rows[i][j] = "";
                                    requetpage.ClientScript.RegisterStartupScript(requetpage.GetType(), "alert", "CallAlert(0);", true);
                                    break;
                                }

                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }

                    }
                }
            }

            if (type == 1)
            {
                //dt.Columns.RemoveAt(10);
                dt.Columns.RemoveAt(11);  //theerayut.s 17/08/2018
                dt.Columns.Remove("วงเงินตามการ์ด(ลิตร)");
                dt.Columns.Remove("ขอเงินสดจำนวน(บาท)");
                dt.Columns.Remove("น้ำมันจำนวน(ลิตร)");
            }
            else if (type == 2)
            {
                dt.Columns.Remove("วงเงินตามการ์ด(ลิตร)");
                dt.Columns.Remove("ขอเงินสดจำนวน(บาท)");
                dt.Columns.Remove("น้ำมันจำนวน(ลิตร)");
                dt.Columns.Remove("สถานที่จอดรถ");
            }
            else if (type == 3)
            {
                dt.Columns.RemoveAt(9);
                dt.Columns["วัน/เวลาที่ยืม"].ColumnName = "วัน/เวลาที่เติม";
                dt.Columns.Remove("สถานที่จอดรถ");
            }

            for (i = 0; i < dt.Rows.Count; i++)
            {
                for (j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j].ToString().Trim() == "")
                    {
                        dt.Rows[i].Delete();
                        i = i - 1;
                        break;
                    }

                }

            }

            return dt;
        }

        public static DataTable FilterDTApprove(DataTable dtSQL, int type, Page requestpage)
        {
            string EmpID_Login = string.Empty;
            DataRow dr1, dr2;

            int i = 0, j = 0;
            DataTable dt = new DataTable();

            dt.Columns.Add("เลือก");
            dt.Columns.Add("เลขที่เอกสาร", typeof(String));
            dt.Columns.Add("ผู้อนุมัติปัจุบัน", typeof(String));
            dt.Columns.Add("ทะเบียนรถ", typeof(String));
            dt.Columns.Add("ยี่ห้อ", typeof(String));
            dt.Columns.Add("วงเงินตามการ์ด(ลิตร)", typeof(String));
            dt.Columns.Add("ขอเงินสดจำนวน(บาท)", typeof(String));
            dt.Columns.Add("น้ำมันจำนวน(ลิตร)", typeof(String));
            dt.Columns.Add("วัน/เวลาที่ยืม", typeof(String));
            dt.Columns.Add("วัน/เวลาที่คืน", typeof(String));
            dt.Columns.Add("เหตุผลในการยืม", typeof(String));
            dt.Columns.Add("สถานที่จอดรถ", typeof(String));

            if (dtSQL.Rows.Count > 0)
            {

                for (i = 0; i < dtSQL.Rows.Count; i++)

                {

                    dr1 = dt.NewRow();
                    dt.Rows.Add(dr1);

                    for (j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 0)
                        {
                            dt.Rows[i][j] = i;
                        }
                        else if (j == 1)
                        {
                            if (dtSQL.Rows[i]["document_id"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["document_id"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 2)
                        {
                            if (dtSQL.Rows[i]["Approver_By"].ToString() != "0" && dtSQL.Rows[i]["Approver_By"].ToString() != "")
                            {
                                DataTable ApproverInfo = LoginThaiBev.EmpInfo.GetEmployeeINFO(dtSQL.Rows[i]["Approver_By"].ToString());
                                string ApproverName = ApproverInfo.Rows[0]["LocalFirstName"].ToString() + "  " + ApproverInfo.Rows[0]["localLastName"].ToString();
                                dt.Rows[i][j] = ApproverName;
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 3)
                        {
                            if (dtSQL.Rows[i]["Plate_Car"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Plate_Car"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 4)
                        {
                            if (dtSQL.Rows[i]["Band_Car"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Band_Car"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }
                        }
                        else if (j == 5)
                        {
                            if (dtSQL.Rows[i]["Oil_Limit"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_Limit"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 6)
                        {
                            if (dtSQL.Rows[i]["Oil_Cash"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_Cash"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 7)
                        {
                            if (dtSQL.Rows[i]["Oil_litre"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Oil_litre"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }
                        else if (j == 8 || j == 9)
                        {
                            if (dtSQL.Rows[i]["Start_Date"].ToString() != "")
                            {
                                if (type != 3)
                                {
                                    if (dtSQL.Rows[i]["EndTime"].ToString() != "" && dtSQL.Rows[i]["End_Date"].ToString() != "")
                                    {
                                        if (j == 8)
                                        {
                                            DateTime fullstarttime = DateTime.Parse(dtSQL.Rows[i]["Start_Date"].ToString()).Date.Add(DateTime.Parse(dtSQL.Rows[i]["StartTime"].ToString()).TimeOfDay);

                                            dt.Rows[i][j] = fullstarttime.ToString("dd/MM/yyyy hh:mm");
                                        }
                                        else
                                        {
                                            DateTime fullendtime = DateTime.Parse(dtSQL.Rows[i]["End_Date"].ToString()).Date.Add(DateTime.Parse(dtSQL.Rows[i]["EndTime"].ToString()).TimeOfDay);

                                            dt.Rows[i][j] = fullendtime.ToString("dd/MM/yyyy hh:mm");
                                        }
                                    }
                                    else
                                    {
                                        dt.Rows[i][j] = "";
                                        requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                        break;
                                    }
                                }
                                else
                                {
                                    if (j == 8)
                                    {
                                        dt.Rows[i][j] = DateTime.Parse(dtSQL.Rows[i]["Start_Date"].ToString()).ToString("dd/MM/yyy");
                                    }
                                    else
                                    {
                                        dt.Rows[i][j] = "";
                                    }
                                }
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                                requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                break;
                            }

                        }

                        else if (j == 10)
                        {
                            if (type != 1)
                            {
                                if (dtSQL.Rows[i]["Reason"].ToString() != "")
                                {
                                    dt.Rows[i][j] = dtSQL.Rows[i]["Reason"].ToString();
                                }
                                else
                                {
                                    dt.Rows[i][j] = "";
                                    requestpage.ClientScript.RegisterStartupScript(requestpage.GetType(), "alert", "CallAlert(0);", true);
                                    break;
                                }

                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }

                        }

                        else
                        {
                            if (dtSQL.Rows[i]["Parking_Address"].ToString() != "")
                            {
                                dt.Rows[i][j] = dtSQL.Rows[i]["Parking_Address"].ToString();
                            }
                            else
                            {
                                dt.Rows[i][j] = "";
                            }
                        }

                    }
                }
            }

            if (type == 1)
            {
                dt.Columns.RemoveAt(10);
                dt.Columns.Remove("วงเงินตามการ์ด(ลิตร)");
                dt.Columns.Remove("ขอเงินสดจำนวน(บาท)");
                dt.Columns.Remove("น้ำมันจำนวน(ลิตร)");
            }
            else if (type == 2)
            {
                dt.Columns.Remove("สถานที่จอดรถ");
                dt.Columns.Remove("วงเงินตามการ์ด(ลิตร)");
                dt.Columns.Remove("ขอเงินสดจำนวน(บาท)");
                dt.Columns.Remove("น้ำมันจำนวน(ลิตร)");
            }
            else if (type == 3)
            {
                dt.Columns.RemoveAt(9);
                dt.Columns["วัน/เวลาที่ยืม"].ColumnName = "วัน/เวลาที่เติม";
                dt.Columns.Remove("สถานที่จอดรถ");
            }

            for (i = 0; i < dt.Rows.Count; i++)
            {
                for (j = 1; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j].ToString().Trim() == "")
                    {
                        dt.Rows[i].Delete();
                        i = i - 1;
                        break;
                    }

                }

            }

            return dt;
        }

        public static string findmonthname(string numbermonth)
        {
            string monthname = "";
            switch (numbermonth)
            {
                case "01":
                    monthname = "มกราคม";
                    break;
                case "02":
                    monthname = "กุมภาพันธ์";
                    break;
                case "03":
                    monthname = "มีนาคม";
                    break;
                case "04":
                    monthname = "เมษายน";
                    break;
                case "05":
                    monthname = "พฤษภาคม";
                    break;
                case "06":
                    monthname = "มิถุนายน";
                    break;
                case "07":
                    monthname = "กรกฏาคม";
                    break;
                case "08":
                    monthname = "สิงหาคม";
                    break;
                case "09":
                    monthname = "กันยายน";
                    break;
                case "10":
                    monthname = "ตุลาคม";
                    break;
                case "11":
                    monthname = "พฤศจิกายน";
                    break;
                case "12":
                    monthname = "ธันวาคม";
                    break;
            }

            return monthname;
        }

        #region Reports

        //รายงานอัตราการสิ้นเปลืองประจำเดือน --> ระยะทางในการใช้งาน / จำนวนน้ำมันที่ใช้ / อัตราสิ้นเปลือง : เดือน
        public static DataTable GET_Summary_Monthly_Report(string EmpID_Login, string EmpID_Search, string Plate_No, string Card_No, string Monthly, string Region, string Compcode, string Province_Car)
        {
            //Used get region: 1 --> GET_Summary_Monthly_Report("X","X","X","X","X","1","X","X")
            Region = "1";

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Summary_Monthly_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Login;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Search;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@LicensePlate_Car", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = Plate_No;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@FleetCardNo", SqlDbType.NVarChar);
                dCmd.Parameters[3].Value = Card_No;  //ASS_2019_R1_NUM
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Monthly", SqlDbType.NVarChar);
                dCmd.Parameters[4].Value = Monthly;  //R1 : ภาค 1
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Region", SqlDbType.NVarChar);
                dCmd.Parameters[5].Value = Region;  //R1 : ภาค 1
                dCmd.Parameters[5].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Compcode", SqlDbType.NVarChar);
                dCmd.Parameters[6].Value = Compcode;  //R1 : ภาค 1
                dCmd.Parameters[6].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Province_Cars", SqlDbType.NVarChar);
                dCmd.Parameters[7].Value = Province_Car;  //R1 : ภาค 1
                dCmd.Parameters[7].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        //สรุป --> วันเวลาที่ ยืม/คืน รถ : ทุกวันในเดือน
        public static DataTable GET_Trans_Borrowing_Monthly_Report(string EmpID_Login, string EmpID_Search, string EmpName_Search, string Plate_No, string Card_No, string Monthly, string Region, string Compcode, string Province_Car)
        {
            //Used get region: 1 --> GET_Trans_Borrowing_Monthly_Report("X","X","X","X","X","1","X","X")

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Trans_Borrowing_Monthly_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Login;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Search;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpName", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = EmpName_Search;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@LicensePlate_Car", SqlDbType.NVarChar);
                dCmd.Parameters[3].Value = Plate_No;  //2019
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@FleetCardNo", SqlDbType.NVarChar);
                dCmd.Parameters[4].Value = Card_No;  //ASS_2019_R1_NUM
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Monthly", SqlDbType.NVarChar);
                dCmd.Parameters[5].Value = Monthly;  //R1 : ภาค 1
                dCmd.Parameters[5].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Region", SqlDbType.NVarChar);
                dCmd.Parameters[6].Value = Region;  //R1 : ภาค 1
                dCmd.Parameters[6].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Compcode", SqlDbType.NVarChar);
                dCmd.Parameters[7].Value = Compcode;  //R1 : ภาค 1
                dCmd.Parameters[7].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Province_Cars", SqlDbType.NVarChar);
                dCmd.Parameters[8].Value = Province_Car;  //R1 : ภาค 1
                dCmd.Parameters[8].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        //สรุป --> วันเวลาที่ ยืม/คืน บัตร : ทุกวันในเดือน
        public static DataTable GET_Trans_Card_Borrowing_Monthly_Report(string EmpID_Login, string EmpID_Search, string EmpName_Search, string Plate_No, string Card_No, string Monthly, string Region, string Compcode, string Province_Car)
        {
            //Used get region: 1 --> GET_Trans_Borrowing_Monthly_Report("X","X","X","X","X","1","X","X")

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Trans_Card_Borrowing_Monthly_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Login;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Search;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpName", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = EmpName_Search;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@LicensePlate_Car", SqlDbType.NVarChar);
                dCmd.Parameters[3].Value = Plate_No;  //2019
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@FleetCardNo", SqlDbType.NVarChar);
                dCmd.Parameters[4].Value = Card_No;  //ASS_2019_R1_NUM
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Monthly", SqlDbType.NVarChar);
                dCmd.Parameters[5].Value = Monthly;  //R1 : ภาค 1
                dCmd.Parameters[5].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Region", SqlDbType.NVarChar);
                dCmd.Parameters[6].Value = Region;  //R1 : ภาค 1
                dCmd.Parameters[6].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Compcode", SqlDbType.NVarChar);
                dCmd.Parameters[7].Value = Compcode;  //R1 : ภาค 1
                dCmd.Parameters[7].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Province_Cars", SqlDbType.NVarChar);
                dCmd.Parameters[8].Value = Province_Car;  //R1 : ภาค 1
                dCmd.Parameters[8].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        //สรุป --> การใช้บัตร จำนวนลิตร / จำนวนเงินที่จ่าย : ทุกวันในเดือน
        public static DataTable GET_Trans_Cards_Monthly_Report(string EmpID_Login, string EmpID_Search, string EmpName_Search, string Plate_No, string Card_No, string Monthly, string Region, string Compcode, string Province_Car)
        {
            //Used get region: 1 --> GET_Trans_Cards_Monthly_Report("X","X","X","X","X","1","X","X")

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Trans_Cards_Monthly_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Login;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Search;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpName", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = EmpName_Search;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@LicensePlate_Car", SqlDbType.NVarChar);
                dCmd.Parameters[3].Value = Plate_No;  //2019
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@FleetCardNo", SqlDbType.NVarChar);
                dCmd.Parameters[4].Value = Card_No;  //ASS_2019_R1_NUM
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Monthly", SqlDbType.NVarChar);
                dCmd.Parameters[5].Value = Monthly;  //R1 : ภาค 1
                dCmd.Parameters[5].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Region", SqlDbType.NVarChar);
                dCmd.Parameters[6].Value = Region;  //R1 : ภาค 1
                dCmd.Parameters[6].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Compcode", SqlDbType.NVarChar);
                dCmd.Parameters[7].Value = Compcode;  //R1 : ภาค 1
                dCmd.Parameters[7].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Province_Cars", SqlDbType.NVarChar);
                dCmd.Parameters[8].Value = Province_Car;  //R1 : ภาค 1
                dCmd.Parameters[8].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        //สรุป --> การใช้รถ ระยะไมล์เริ่มต้น / ระยะไมล์สิ้นสุด / ระยะทางทั้งหมดที่ใช้งานในแต่หล่ะวัน : ทุกวันในเดือน
        public static DataTable GET_Trans_Cars_Monthly_Report(string EmpID_Login, string EmpID_Search, string EmpName_Search, string Plate_No, string Card_No, string Monthly, string Region, string Compcode, string Province_Car)
        {
            //Used get region: 1 --> GET_Trans_Cars_Monthly_Report("X","X","X","X","X","1","X","X")

            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Trans_Cars_Monthly_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Login", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Login;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Search;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;


                dCmd.Parameters.Add("@EmpName", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = EmpName_Search;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@LicensePlate_Car", SqlDbType.NVarChar);
                dCmd.Parameters[3].Value = Plate_No;  //2019
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@FleetCardNo", SqlDbType.NVarChar);
                dCmd.Parameters[4].Value = Card_No;  //ASS_2019_R1_NUM
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Monthly", SqlDbType.NVarChar);
                dCmd.Parameters[5].Value = Monthly;  //R1 : ภาค 1
                dCmd.Parameters[5].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Region", SqlDbType.NVarChar);
                dCmd.Parameters[6].Value = Region;  //R1 : ภาค 1
                dCmd.Parameters[6].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Compcode", SqlDbType.NVarChar);
                dCmd.Parameters[7].Value = Compcode;  //R1 : ภาค 1
                dCmd.Parameters[7].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Province_Cars", SqlDbType.NVarChar);
                dCmd.Parameters[8].Value = Province_Car;  //R1 : ภาค 1
                dCmd.Parameters[8].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);

            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        //สรุป --> การร้องขออนุมัติการใช้งานต่างๆ
        public static DataTable GET_Doc_Approve_Workflow_Report(string EmpID_Approver, string EmpID_Requestor, string Document_id, int Status, int ProcessID)
        {
            string connStr = ConfigurationManager.ConnectionStrings["DBFleetConnect"].ConnectionString;
            DataSet dsresult = new DataSet();
            DataSet ds = new DataSet();
            DataTable DT_Main = new DataTable();
            DataTable DT = new DataTable();

            SqlConnection conn = new SqlConnection();
            SqlCommand dCmd = new SqlCommand();

            try
            {

                string strSql = @"[dbo].[GET_Doc_Approve_Workflow_Report]";

                conn = new SqlConnection(connStr);
                conn.Open();
                dCmd = new SqlCommand(strSql, conn);
                dCmd.CommandTimeout = 1200;
                dCmd.CommandType = CommandType.StoredProcedure;
                dCmd.Parameters.Add("@EmpID_Approver", SqlDbType.NVarChar);
                dCmd.Parameters[0].Value = EmpID_Approver;  //1: ASSET | 2: EXPANE
                dCmd.Parameters[0].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@EmpID_Requestor", SqlDbType.NVarChar);
                dCmd.Parameters[1].Value = EmpID_Requestor;  //2019
                dCmd.Parameters[1].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Document_id", SqlDbType.NVarChar);
                dCmd.Parameters[2].Value = Document_id;  //2019
                dCmd.Parameters[2].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@Status", SqlDbType.Int);
                dCmd.Parameters[3].Value = Status;  //ASS_2019_R1_NUM
                dCmd.Parameters[3].Direction = ParameterDirection.Input;

                dCmd.Parameters.Add("@ProcessID", SqlDbType.Int);
                dCmd.Parameters[4].Value = ProcessID;  //R1 : ภาค 1
                dCmd.Parameters[4].Direction = ParameterDirection.Input;

                var adapter = new SqlDataAdapter(dCmd);

                adapter.Fill(DT);
               
            }
            catch
            {
                throw;
            }
            finally
            {
                dCmd.Dispose();
                conn.Close();
                conn.Dispose();
            }

            return DT;
        }

        #endregion

    }
}